// Copyright © 2023 TXA PTE. LTD.

package handler

import (
	"context"
	"fmt"
	"math/big"
	"strconv"
	"time"

	"sdp/api"
	"sdp/model"
	"sdp/store"

	"github.com/go-redis/redis/v8"
	"github.com/lib/pq"
	"gorm.io/gorm"
)

func (h *Handler) ProcessTrades(ctx context.Context) {
	ts := store.NewTradeStore(h.db)
	largestReceivedTrade := ts.MostRecentTrade()

	duration := tickerDuration

	ticker := time.NewTicker(duration)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			foundTrades, err := h.processMissedTrades(ctx, ts, largestReceivedTrade)
			if err != nil {
				h.logException(err)
				return
			}
			if err := h.broadcastTradeRequest(ts); err != nil {
				h.logException(err)
				return
			}
			if !foundTrades && duration <= maxTickerDuration {
				duration = duration * 2
				ticker.Reset(duration)
			}
		case lis := <-h.tradeChan:
			if err := h.processNewTrade(ctx, lis, ts, &largestReceivedTrade); err != nil {
				h.logException(err)
				return
			}
			duration = tickerDuration
			ticker.Reset(duration)
		}
	}
}

func (h *Handler) processMissedTrades(ctx context.Context, ts *store.TradeStore, largestReceivedTrade uint64) (bool, error) {
	lastTrade, err := ts.GetLastProcessedTrade()
	if err != nil {
		return false, err
	}

	foundMissingTrade := false
	for nextSequence := lastTrade.SdpSequenceNumber + 1; nextSequence <= largestReceivedTrade; nextSequence++ {
		trade, err := h.getNextTrade(nextSequence, ts)
		if err != nil {
			return true, err
		}
		if err := h.processTrade(trade); err != nil {
			return true, err
		}
		foundMissingTrade = true
	}
	return foundMissingTrade, nil
}

func (h *Handler) processNewTrade(ctx context.Context, lis *pq.Notification, ts *store.TradeStore, largestReceivedTrade *uint64) error {
	sequenceNumber, err := strconv.ParseUint(lis.Extra, 10, 64)
	if err != nil {
		return err
	}

	lastTrade, err := ts.GetLastProcessedTrade()
	if err != nil {
		return err
	}
	if sequenceNumber > *largestReceivedTrade {
		*largestReceivedTrade = sequenceNumber
	}

	trade, err := h.getNextTrade(lastTrade.SdpSequenceNumber+1, ts)
	if err != nil {
		return err
	}

	return h.processTrade(trade)
}

func (h *Handler) broadcastTradeRequest(ts *store.TradeStore) error {
	lastTrade, err := ts.GetLastProcessedTrade()
	if err != nil {
		return err
	}

	seq := lastTrade.SdpSequenceNumber + 1
	h.controller.Broadcast(seq)
	h.log.Infof("sending broadcast for trade sequence: %d", seq)

	return nil
}

func (h *Handler) getNextTrade(nextTradeId uint64, store *store.TradeStore) (*model.Trade, error) {
	trade, err := store.GetBySeq(nextTradeId)
	if err == nil && trade != nil {
		return trade, nil
	}

	s, err := h.queue.Get(context.Background(), fmt.Sprintf("trade_%d", nextTradeId))
	if err == nil {
		trade, err = trade.UnmarshalBinary([]byte(s))
		if err != nil {
			return nil, err
		}
		return nil, store.Create(trade)
	}

	if err != redis.Nil {
		return nil, err
	}

	return nil, nil
}

func (h *Handler) processTrade(trade *model.Trade) error {
	if trade == nil {
		return nil
	}
	if trade.Processed {
		return nil
	}

	ts := store.NewTradeStore(h.db)
	lastProcessed, err := ts.GetLastProcessedTrade()
	if err != nil {
		return err
	}
	if trade.SdpSequenceNumber != lastProcessed.SdpSequenceNumber+1 {
		return nil
	}

	product, ok := api.GetProducts()[trade.Product]
	if !ok {
		return fmt.Errorf("cannot get trade product info for %s", trade.Product)
	}
	sizeBigInt, ok := big.NewInt(0).SetString(trade.Size, 10)
	if !ok {
		return fmt.Errorf("cannot parse size, got %s but wanted number", trade.Size)
	}
	priceBigInt, ok := big.NewInt(0).SetString(trade.Price, 10)
	if !ok {
		return fmt.Errorf("cannot parse size, got %s but wanted number", trade.Price)
	}
	factorToDivide := big.NewInt(int64(product.GetBaseAssetPrecision()))
	buyerOwesCounter := big.NewInt(0).Mul(sizeBigInt, priceBigInt)

	exp := big.NewInt(0).Exp(big.NewInt(10), factorToDivide, nil)
	buyerOwesCounterFinal := big.NewInt(0).Div(buyerOwesCounter, exp)

	var tries uint = 0
	var retryAttempts uint = 15
	for tries <= retryAttempts {
		err := h.db.Transaction(func(tx *gorm.DB) error {
			rejected, err := ts.IsRejected(trade.SdpSequenceNumber)
			if err != nil {
				return err
			}
			if rejected {
				if err := ts.UpdateLastProcessedTrade(trade.SdpSequenceNumber); err != nil {
					return err
				}
				if err := ts.ProcessTrade(trade); err != nil {
					return err
				}
				h.log.Warnf("trade %d was rejected, skipping it", trade.SdpSequenceNumber)
				return nil
			}
			// Buyer pays seller counter asset
			if err := h.payAccount(
				tx,
				trade.GetBuyerCounterAddress(),
				trade.GetSellerCounterAddress(),
				product.GetCounterAsset(),
				product.GetCounterChainId(),
				buyerOwesCounterFinal,
			); err != nil {
				return err
			}
			// Seller pays buyer base asset
			if err := h.payAccount(
				tx,
				trade.GetSellerBaseAddress(),
				trade.GetBuyerBaseAddress(),
				product.GetBaseAsset(),
				product.GetBaseChainId(),
				sizeBigInt,
			); err != nil {
				h.log.Error(err)
				return err
			}
			ts := store.NewTradeStore(tx)
			if err := ts.UpdateLastProcessedTrade(trade.SdpSequenceNumber); err != nil {
				return err
			}
			if err := ts.ProcessTrade(trade); err != nil {
				return err
			}
			h.log.Infof("finished processing trade: %d", trade.SdpSequenceNumber)
			return nil
		})
		if err == nil {
			return nil
		}
		h.log.Error(err)
		time.Sleep(time.Second * 2)
		tries++
	}
	if tries >= retryAttempts {
		if err := ts.UpdateLastProcessedTrade(trade.SdpSequenceNumber); err != nil {
			return err
		}
		if err := ts.ProcessTrade(trade); err != nil {
			return err
		}
		h.log.Warnf("skipping trade: %d", trade.SdpSequenceNumber)
	}
	return nil
}
