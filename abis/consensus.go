// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package abis

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// SettlementDataConsensusExchangeReport is an auto generated low-level Go binding around an user-defined struct.
type SettlementDataConsensusExchangeReport struct {
	MerkleRoot   [32]byte
	SettlementId *big.Int
	Coordinator  common.Address
}

// SettlementLibObligation is an auto generated low-level Go binding around an user-defined struct.
type SettlementLibObligation struct {
	Amount     *big.Int
	Deliverer  common.Address
	Recipient  common.Address
	Token      common.Address
	Reallocate bool
}

// SettlementUtilitiesCollateralTransfer is an auto generated low-level Go binding around an user-defined struct.
type SettlementUtilitiesCollateralTransfer struct {
	Party  common.Address
	Amount *big.Int
}

// SettlementUtilitiesTransferObligation is an auto generated low-level Go binding around an user-defined struct.
type SettlementUtilitiesTransferObligation struct {
	Sender    common.Address
	Recipient common.Address
	Amount    *big.Int
}

// ConsensusMetaData contains all meta data concerning the Consensus contract.
var ConsensusMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_identity\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"coordinator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"sdp\",\"type\":\"address\"}],\"name\":\"ObligationsReported\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"settlementId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"correctRoot\",\"type\":\"bytes32\"}],\"name\":\"QuorumDisputeSettledExchange\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"settlementId\",\"type\":\"uint256\"}],\"name\":\"SettlementConcurredExchange\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"settlementId\",\"type\":\"uint256\"}],\"name\":\"SettlementDisputedExchange\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"settlementId\",\"type\":\"uint256\"}],\"name\":\"SettlementDisputedQuorum\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"startBlock\",\"type\":\"uint256\"}],\"name\":\"calculateDifficulty\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"deliverer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"reallocate\",\"type\":\"bool\"}],\"internalType\":\"structSettlementLib.Obligation[]\",\"name\":\"correct\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"deliverer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"reallocate\",\"type\":\"bool\"}],\"internalType\":\"structSettlementLib.Obligation[]\",\"name\":\"reported\",\"type\":\"tuple[]\"}],\"name\":\"compareObligationAmounts\",\"outputs\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"party\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"internalType\":\"structSettlementUtilities.CollateralTransfer[]\",\"name\":\"compensations\",\"type\":\"tuple[]\"}],\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"deliverer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"reallocate\",\"type\":\"bool\"}],\"internalType\":\"structSettlementLib.Obligation[]\",\"name\":\"reported\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"deliverer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"reallocate\",\"type\":\"bool\"}],\"internalType\":\"structSettlementLib.Obligation[]\",\"name\":\"existing\",\"type\":\"tuple[]\"}],\"name\":\"compareObligations\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"party\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"internalType\":\"structSettlementUtilities.CollateralTransfer[]\",\"name\":\"senders\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"party\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"internalType\":\"structSettlementUtilities.CollateralTransfer[]\",\"name\":\"recipients\",\"type\":\"tuple[]\"}],\"name\":\"generateTransferObligations\",\"outputs\":[{\"components\":[{\"internalType\":\"address\",\"name\":\"sender\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"internalType\":\"structSettlementUtilities.TransferObligation[]\",\"name\":\"slashObligations\",\"type\":\"tuple[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32[]\",\"name\":\"leaves\",\"type\":\"bytes32[]\"}],\"name\":\"getMerkleRoot\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"coordinator\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"settlementId\",\"type\":\"uint256\"}],\"name\":\"getObligationsId\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"id\",\"type\":\"bytes32\"}],\"name\":\"getReportedObligations\",\"outputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"deliverer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"reallocate\",\"type\":\"bool\"}],\"internalType\":\"structSettlementLib.Obligation[]\",\"name\":\"\",\"type\":\"tuple[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"id\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"sdp\",\"type\":\"address\"}],\"name\":\"hasReported\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"maxBlocks\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"maxDifficulty\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"minDifficulty\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"minQuorum\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"bytes32\",\"name\":\"merkleRoot\",\"type\":\"bytes32\"},{\"internalType\":\"uint256\",\"name\":\"settlementId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"coordinator\",\"type\":\"address\"}],\"internalType\":\"structSettlementDataConsensus.ExchangeReport\",\"name\":\"exchangeReport\",\"type\":\"tuple\"},{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"name\":\"reportExchangeMerkleRootConcur\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"deliverer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"reallocate\",\"type\":\"bool\"}],\"internalType\":\"structSettlementLib.Obligation[]\",\"name\":\"reportedObligations\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"bytes32\",\"name\":\"merkleRoot\",\"type\":\"bytes32\"},{\"internalType\":\"uint256\",\"name\":\"settlementId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"coordinator\",\"type\":\"address\"}],\"internalType\":\"structSettlementDataConsensus.ExchangeReport\",\"name\":\"exchangeReport\",\"type\":\"tuple\"},{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"name\":\"reportExchangeMerkleRootDispute\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"deliverer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"reallocate\",\"type\":\"bool\"}],\"internalType\":\"structSettlementLib.Obligation[]\",\"name\":\"reportedObligations\",\"type\":\"tuple[]\"},{\"components\":[{\"internalType\":\"bytes32\",\"name\":\"merkleRoot\",\"type\":\"bytes32\"},{\"internalType\":\"uint256\",\"name\":\"settlementId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"coordinator\",\"type\":\"address\"}],\"internalType\":\"structSettlementDataConsensus.ExchangeReport\",\"name\":\"exchangeReport\",\"type\":\"tuple\"},{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"name\":\"reportMerkleRootQuorumDisputed\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"coordinator\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"settlementId\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"deliverer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"reallocate\",\"type\":\"bool\"}],\"internalType\":\"structSettlementLib.Obligation[]\",\"name\":\"reportedObligations\",\"type\":\"tuple[]\"},{\"internalType\":\"bytes32\",\"name\":\"merkleRoot\",\"type\":\"bytes32\"},{\"internalType\":\"uint256\",\"name\":\"nonce\",\"type\":\"uint256\"}],\"name\":\"reportSettlementObligations\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"name\":\"settlements\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"quorum\",\"type\":\"uint256\"},{\"internalType\":\"enumSettlementDataConsensus.SettlementStage\",\"name\":\"stage\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"merkleRoot\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"nonce\",\"type\":\"uint256\"},{\"internalType\":\"bytes32\",\"name\":\"merkleRoot\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"reporter\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"settlementId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"lastReporter\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"startBlock\",\"type\":\"uint256\"}],\"name\":\"validateNonce\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"work\",\"type\":\"bytes32\"},{\"internalType\":\"uint256\",\"name\":\"difficulty\",\"type\":\"uint256\"}],\"name\":\"validateWork\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"deliverer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"reallocate\",\"type\":\"bool\"}],\"internalType\":\"structSettlementLib.Obligation[]\",\"name\":\"obligations\",\"type\":\"tuple[]\"}],\"name\":\"verifyMerkleTree\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"pure\",\"type\":\"function\"}]",
}

// ConsensusABI is the input ABI used to generate the binding from.
// Deprecated: Use ConsensusMetaData.ABI instead.
var ConsensusABI = ConsensusMetaData.ABI

// Consensus is an auto generated Go binding around an Ethereum contract.
type Consensus struct {
	ConsensusCaller     // Read-only binding to the contract
	ConsensusTransactor // Write-only binding to the contract
	ConsensusFilterer   // Log filterer for contract events
}

// ConsensusCaller is an auto generated read-only Go binding around an Ethereum contract.
type ConsensusCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ConsensusTransactor is an auto generated write-only Go binding around an Ethereum contract.
type ConsensusTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ConsensusFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type ConsensusFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ConsensusSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type ConsensusSession struct {
	Contract     *Consensus        // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ConsensusCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type ConsensusCallerSession struct {
	Contract *ConsensusCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts    // Call options to use throughout this session
}

// ConsensusTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type ConsensusTransactorSession struct {
	Contract     *ConsensusTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts    // Transaction auth options to use throughout this session
}

// ConsensusRaw is an auto generated low-level Go binding around an Ethereum contract.
type ConsensusRaw struct {
	Contract *Consensus // Generic contract binding to access the raw methods on
}

// ConsensusCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type ConsensusCallerRaw struct {
	Contract *ConsensusCaller // Generic read-only contract binding to access the raw methods on
}

// ConsensusTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type ConsensusTransactorRaw struct {
	Contract *ConsensusTransactor // Generic write-only contract binding to access the raw methods on
}

// NewConsensus creates a new instance of Consensus, bound to a specific deployed contract.
func NewConsensus(address common.Address, backend bind.ContractBackend) (*Consensus, error) {
	contract, err := bindConsensus(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Consensus{ConsensusCaller: ConsensusCaller{contract: contract}, ConsensusTransactor: ConsensusTransactor{contract: contract}, ConsensusFilterer: ConsensusFilterer{contract: contract}}, nil
}

// NewConsensusCaller creates a new read-only instance of Consensus, bound to a specific deployed contract.
func NewConsensusCaller(address common.Address, caller bind.ContractCaller) (*ConsensusCaller, error) {
	contract, err := bindConsensus(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &ConsensusCaller{contract: contract}, nil
}

// NewConsensusTransactor creates a new write-only instance of Consensus, bound to a specific deployed contract.
func NewConsensusTransactor(address common.Address, transactor bind.ContractTransactor) (*ConsensusTransactor, error) {
	contract, err := bindConsensus(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &ConsensusTransactor{contract: contract}, nil
}

// NewConsensusFilterer creates a new log filterer instance of Consensus, bound to a specific deployed contract.
func NewConsensusFilterer(address common.Address, filterer bind.ContractFilterer) (*ConsensusFilterer, error) {
	contract, err := bindConsensus(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &ConsensusFilterer{contract: contract}, nil
}

// bindConsensus binds a generic wrapper to an already deployed contract.
func bindConsensus(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(ConsensusABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Consensus *ConsensusRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Consensus.Contract.ConsensusCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Consensus *ConsensusRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Consensus.Contract.ConsensusTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Consensus *ConsensusRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Consensus.Contract.ConsensusTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Consensus *ConsensusCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Consensus.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Consensus *ConsensusTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Consensus.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Consensus *ConsensusTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Consensus.Contract.contract.Transact(opts, method, params...)
}

// CalculateDifficulty is a free data retrieval call binding the contract method 0x88dbbe34.
//
// Solidity: function calculateDifficulty(uint256 startBlock) view returns(uint256)
func (_Consensus *ConsensusCaller) CalculateDifficulty(opts *bind.CallOpts, startBlock *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "calculateDifficulty", startBlock)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CalculateDifficulty is a free data retrieval call binding the contract method 0x88dbbe34.
//
// Solidity: function calculateDifficulty(uint256 startBlock) view returns(uint256)
func (_Consensus *ConsensusSession) CalculateDifficulty(startBlock *big.Int) (*big.Int, error) {
	return _Consensus.Contract.CalculateDifficulty(&_Consensus.CallOpts, startBlock)
}

// CalculateDifficulty is a free data retrieval call binding the contract method 0x88dbbe34.
//
// Solidity: function calculateDifficulty(uint256 startBlock) view returns(uint256)
func (_Consensus *ConsensusCallerSession) CalculateDifficulty(startBlock *big.Int) (*big.Int, error) {
	return _Consensus.Contract.CalculateDifficulty(&_Consensus.CallOpts, startBlock)
}

// CompareObligationAmounts is a free data retrieval call binding the contract method 0xef80bd3e.
//
// Solidity: function compareObligationAmounts((uint256,address,address,address,bool)[] correct, (uint256,address,address,address,bool)[] reported) pure returns((address,uint256)[] compensations)
func (_Consensus *ConsensusCaller) CompareObligationAmounts(opts *bind.CallOpts, correct []SettlementLibObligation, reported []SettlementLibObligation) ([]SettlementUtilitiesCollateralTransfer, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "compareObligationAmounts", correct, reported)

	if err != nil {
		return *new([]SettlementUtilitiesCollateralTransfer), err
	}

	out0 := *abi.ConvertType(out[0], new([]SettlementUtilitiesCollateralTransfer)).(*[]SettlementUtilitiesCollateralTransfer)

	return out0, err

}

// CompareObligationAmounts is a free data retrieval call binding the contract method 0xef80bd3e.
//
// Solidity: function compareObligationAmounts((uint256,address,address,address,bool)[] correct, (uint256,address,address,address,bool)[] reported) pure returns((address,uint256)[] compensations)
func (_Consensus *ConsensusSession) CompareObligationAmounts(correct []SettlementLibObligation, reported []SettlementLibObligation) ([]SettlementUtilitiesCollateralTransfer, error) {
	return _Consensus.Contract.CompareObligationAmounts(&_Consensus.CallOpts, correct, reported)
}

// CompareObligationAmounts is a free data retrieval call binding the contract method 0xef80bd3e.
//
// Solidity: function compareObligationAmounts((uint256,address,address,address,bool)[] correct, (uint256,address,address,address,bool)[] reported) pure returns((address,uint256)[] compensations)
func (_Consensus *ConsensusCallerSession) CompareObligationAmounts(correct []SettlementLibObligation, reported []SettlementLibObligation) ([]SettlementUtilitiesCollateralTransfer, error) {
	return _Consensus.Contract.CompareObligationAmounts(&_Consensus.CallOpts, correct, reported)
}

// CompareObligations is a free data retrieval call binding the contract method 0xb2a8da96.
//
// Solidity: function compareObligations((uint256,address,address,address,bool)[] reported, (uint256,address,address,address,bool)[] existing) pure returns(bool)
func (_Consensus *ConsensusCaller) CompareObligations(opts *bind.CallOpts, reported []SettlementLibObligation, existing []SettlementLibObligation) (bool, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "compareObligations", reported, existing)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// CompareObligations is a free data retrieval call binding the contract method 0xb2a8da96.
//
// Solidity: function compareObligations((uint256,address,address,address,bool)[] reported, (uint256,address,address,address,bool)[] existing) pure returns(bool)
func (_Consensus *ConsensusSession) CompareObligations(reported []SettlementLibObligation, existing []SettlementLibObligation) (bool, error) {
	return _Consensus.Contract.CompareObligations(&_Consensus.CallOpts, reported, existing)
}

// CompareObligations is a free data retrieval call binding the contract method 0xb2a8da96.
//
// Solidity: function compareObligations((uint256,address,address,address,bool)[] reported, (uint256,address,address,address,bool)[] existing) pure returns(bool)
func (_Consensus *ConsensusCallerSession) CompareObligations(reported []SettlementLibObligation, existing []SettlementLibObligation) (bool, error) {
	return _Consensus.Contract.CompareObligations(&_Consensus.CallOpts, reported, existing)
}

// GenerateTransferObligations is a free data retrieval call binding the contract method 0x2128b972.
//
// Solidity: function generateTransferObligations((address,uint256)[] senders, (address,uint256)[] recipients) view returns((address,address,uint256)[] slashObligations)
func (_Consensus *ConsensusCaller) GenerateTransferObligations(opts *bind.CallOpts, senders []SettlementUtilitiesCollateralTransfer, recipients []SettlementUtilitiesCollateralTransfer) ([]SettlementUtilitiesTransferObligation, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "generateTransferObligations", senders, recipients)

	if err != nil {
		return *new([]SettlementUtilitiesTransferObligation), err
	}

	out0 := *abi.ConvertType(out[0], new([]SettlementUtilitiesTransferObligation)).(*[]SettlementUtilitiesTransferObligation)

	return out0, err

}

// GenerateTransferObligations is a free data retrieval call binding the contract method 0x2128b972.
//
// Solidity: function generateTransferObligations((address,uint256)[] senders, (address,uint256)[] recipients) view returns((address,address,uint256)[] slashObligations)
func (_Consensus *ConsensusSession) GenerateTransferObligations(senders []SettlementUtilitiesCollateralTransfer, recipients []SettlementUtilitiesCollateralTransfer) ([]SettlementUtilitiesTransferObligation, error) {
	return _Consensus.Contract.GenerateTransferObligations(&_Consensus.CallOpts, senders, recipients)
}

// GenerateTransferObligations is a free data retrieval call binding the contract method 0x2128b972.
//
// Solidity: function generateTransferObligations((address,uint256)[] senders, (address,uint256)[] recipients) view returns((address,address,uint256)[] slashObligations)
func (_Consensus *ConsensusCallerSession) GenerateTransferObligations(senders []SettlementUtilitiesCollateralTransfer, recipients []SettlementUtilitiesCollateralTransfer) ([]SettlementUtilitiesTransferObligation, error) {
	return _Consensus.Contract.GenerateTransferObligations(&_Consensus.CallOpts, senders, recipients)
}

// GetMerkleRoot is a free data retrieval call binding the contract method 0x4869bfb6.
//
// Solidity: function getMerkleRoot(bytes32[] leaves) pure returns(bytes32)
func (_Consensus *ConsensusCaller) GetMerkleRoot(opts *bind.CallOpts, leaves [][32]byte) ([32]byte, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "getMerkleRoot", leaves)

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// GetMerkleRoot is a free data retrieval call binding the contract method 0x4869bfb6.
//
// Solidity: function getMerkleRoot(bytes32[] leaves) pure returns(bytes32)
func (_Consensus *ConsensusSession) GetMerkleRoot(leaves [][32]byte) ([32]byte, error) {
	return _Consensus.Contract.GetMerkleRoot(&_Consensus.CallOpts, leaves)
}

// GetMerkleRoot is a free data retrieval call binding the contract method 0x4869bfb6.
//
// Solidity: function getMerkleRoot(bytes32[] leaves) pure returns(bytes32)
func (_Consensus *ConsensusCallerSession) GetMerkleRoot(leaves [][32]byte) ([32]byte, error) {
	return _Consensus.Contract.GetMerkleRoot(&_Consensus.CallOpts, leaves)
}

// GetObligationsId is a free data retrieval call binding the contract method 0xd750d5e8.
//
// Solidity: function getObligationsId(address coordinator, uint256 settlementId) pure returns(bytes32)
func (_Consensus *ConsensusCaller) GetObligationsId(opts *bind.CallOpts, coordinator common.Address, settlementId *big.Int) ([32]byte, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "getObligationsId", coordinator, settlementId)

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// GetObligationsId is a free data retrieval call binding the contract method 0xd750d5e8.
//
// Solidity: function getObligationsId(address coordinator, uint256 settlementId) pure returns(bytes32)
func (_Consensus *ConsensusSession) GetObligationsId(coordinator common.Address, settlementId *big.Int) ([32]byte, error) {
	return _Consensus.Contract.GetObligationsId(&_Consensus.CallOpts, coordinator, settlementId)
}

// GetObligationsId is a free data retrieval call binding the contract method 0xd750d5e8.
//
// Solidity: function getObligationsId(address coordinator, uint256 settlementId) pure returns(bytes32)
func (_Consensus *ConsensusCallerSession) GetObligationsId(coordinator common.Address, settlementId *big.Int) ([32]byte, error) {
	return _Consensus.Contract.GetObligationsId(&_Consensus.CallOpts, coordinator, settlementId)
}

// GetReportedObligations is a free data retrieval call binding the contract method 0xcf9887bd.
//
// Solidity: function getReportedObligations(bytes32 id) view returns((uint256,address,address,address,bool)[])
func (_Consensus *ConsensusCaller) GetReportedObligations(opts *bind.CallOpts, id [32]byte) ([]SettlementLibObligation, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "getReportedObligations", id)

	if err != nil {
		return *new([]SettlementLibObligation), err
	}

	out0 := *abi.ConvertType(out[0], new([]SettlementLibObligation)).(*[]SettlementLibObligation)

	return out0, err

}

// GetReportedObligations is a free data retrieval call binding the contract method 0xcf9887bd.
//
// Solidity: function getReportedObligations(bytes32 id) view returns((uint256,address,address,address,bool)[])
func (_Consensus *ConsensusSession) GetReportedObligations(id [32]byte) ([]SettlementLibObligation, error) {
	return _Consensus.Contract.GetReportedObligations(&_Consensus.CallOpts, id)
}

// GetReportedObligations is a free data retrieval call binding the contract method 0xcf9887bd.
//
// Solidity: function getReportedObligations(bytes32 id) view returns((uint256,address,address,address,bool)[])
func (_Consensus *ConsensusCallerSession) GetReportedObligations(id [32]byte) ([]SettlementLibObligation, error) {
	return _Consensus.Contract.GetReportedObligations(&_Consensus.CallOpts, id)
}

// HasReported is a free data retrieval call binding the contract method 0x1394e447.
//
// Solidity: function hasReported(bytes32 id, address sdp) view returns(bool)
func (_Consensus *ConsensusCaller) HasReported(opts *bind.CallOpts, id [32]byte, sdp common.Address) (bool, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "hasReported", id, sdp)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// HasReported is a free data retrieval call binding the contract method 0x1394e447.
//
// Solidity: function hasReported(bytes32 id, address sdp) view returns(bool)
func (_Consensus *ConsensusSession) HasReported(id [32]byte, sdp common.Address) (bool, error) {
	return _Consensus.Contract.HasReported(&_Consensus.CallOpts, id, sdp)
}

// HasReported is a free data retrieval call binding the contract method 0x1394e447.
//
// Solidity: function hasReported(bytes32 id, address sdp) view returns(bool)
func (_Consensus *ConsensusCallerSession) HasReported(id [32]byte, sdp common.Address) (bool, error) {
	return _Consensus.Contract.HasReported(&_Consensus.CallOpts, id, sdp)
}

// MaxBlocks is a free data retrieval call binding the contract method 0x2083569e.
//
// Solidity: function maxBlocks() view returns(uint256)
func (_Consensus *ConsensusCaller) MaxBlocks(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "maxBlocks")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MaxBlocks is a free data retrieval call binding the contract method 0x2083569e.
//
// Solidity: function maxBlocks() view returns(uint256)
func (_Consensus *ConsensusSession) MaxBlocks() (*big.Int, error) {
	return _Consensus.Contract.MaxBlocks(&_Consensus.CallOpts)
}

// MaxBlocks is a free data retrieval call binding the contract method 0x2083569e.
//
// Solidity: function maxBlocks() view returns(uint256)
func (_Consensus *ConsensusCallerSession) MaxBlocks() (*big.Int, error) {
	return _Consensus.Contract.MaxBlocks(&_Consensus.CallOpts)
}

// MaxDifficulty is a free data retrieval call binding the contract method 0x9a997453.
//
// Solidity: function maxDifficulty() view returns(uint256)
func (_Consensus *ConsensusCaller) MaxDifficulty(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "maxDifficulty")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MaxDifficulty is a free data retrieval call binding the contract method 0x9a997453.
//
// Solidity: function maxDifficulty() view returns(uint256)
func (_Consensus *ConsensusSession) MaxDifficulty() (*big.Int, error) {
	return _Consensus.Contract.MaxDifficulty(&_Consensus.CallOpts)
}

// MaxDifficulty is a free data retrieval call binding the contract method 0x9a997453.
//
// Solidity: function maxDifficulty() view returns(uint256)
func (_Consensus *ConsensusCallerSession) MaxDifficulty() (*big.Int, error) {
	return _Consensus.Contract.MaxDifficulty(&_Consensus.CallOpts)
}

// MinDifficulty is a free data retrieval call binding the contract method 0xb4a93283.
//
// Solidity: function minDifficulty() view returns(uint256)
func (_Consensus *ConsensusCaller) MinDifficulty(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "minDifficulty")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MinDifficulty is a free data retrieval call binding the contract method 0xb4a93283.
//
// Solidity: function minDifficulty() view returns(uint256)
func (_Consensus *ConsensusSession) MinDifficulty() (*big.Int, error) {
	return _Consensus.Contract.MinDifficulty(&_Consensus.CallOpts)
}

// MinDifficulty is a free data retrieval call binding the contract method 0xb4a93283.
//
// Solidity: function minDifficulty() view returns(uint256)
func (_Consensus *ConsensusCallerSession) MinDifficulty() (*big.Int, error) {
	return _Consensus.Contract.MinDifficulty(&_Consensus.CallOpts)
}

// MinQuorum is a free data retrieval call binding the contract method 0xb5a127e5.
//
// Solidity: function minQuorum() view returns(uint256)
func (_Consensus *ConsensusCaller) MinQuorum(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "minQuorum")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MinQuorum is a free data retrieval call binding the contract method 0xb5a127e5.
//
// Solidity: function minQuorum() view returns(uint256)
func (_Consensus *ConsensusSession) MinQuorum() (*big.Int, error) {
	return _Consensus.Contract.MinQuorum(&_Consensus.CallOpts)
}

// MinQuorum is a free data retrieval call binding the contract method 0xb5a127e5.
//
// Solidity: function minQuorum() view returns(uint256)
func (_Consensus *ConsensusCallerSession) MinQuorum() (*big.Int, error) {
	return _Consensus.Contract.MinQuorum(&_Consensus.CallOpts)
}

// Settlements is a free data retrieval call binding the contract method 0x01bc95e2.
//
// Solidity: function settlements(bytes32 ) view returns(uint256 quorum, uint8 stage, bytes32 merkleRoot)
func (_Consensus *ConsensusCaller) Settlements(opts *bind.CallOpts, arg0 [32]byte) (struct {
	Quorum     *big.Int
	Stage      uint8
	MerkleRoot [32]byte
}, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "settlements", arg0)

	outstruct := new(struct {
		Quorum     *big.Int
		Stage      uint8
		MerkleRoot [32]byte
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Quorum = *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)
	outstruct.Stage = *abi.ConvertType(out[1], new(uint8)).(*uint8)
	outstruct.MerkleRoot = *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)

	return *outstruct, err

}

// Settlements is a free data retrieval call binding the contract method 0x01bc95e2.
//
// Solidity: function settlements(bytes32 ) view returns(uint256 quorum, uint8 stage, bytes32 merkleRoot)
func (_Consensus *ConsensusSession) Settlements(arg0 [32]byte) (struct {
	Quorum     *big.Int
	Stage      uint8
	MerkleRoot [32]byte
}, error) {
	return _Consensus.Contract.Settlements(&_Consensus.CallOpts, arg0)
}

// Settlements is a free data retrieval call binding the contract method 0x01bc95e2.
//
// Solidity: function settlements(bytes32 ) view returns(uint256 quorum, uint8 stage, bytes32 merkleRoot)
func (_Consensus *ConsensusCallerSession) Settlements(arg0 [32]byte) (struct {
	Quorum     *big.Int
	Stage      uint8
	MerkleRoot [32]byte
}, error) {
	return _Consensus.Contract.Settlements(&_Consensus.CallOpts, arg0)
}

// ValidateNonce is a free data retrieval call binding the contract method 0x0ad50f40.
//
// Solidity: function validateNonce(uint256 nonce, bytes32 merkleRoot, address reporter, uint256 settlementId, address lastReporter, uint256 startBlock) view returns(bool)
func (_Consensus *ConsensusCaller) ValidateNonce(opts *bind.CallOpts, nonce *big.Int, merkleRoot [32]byte, reporter common.Address, settlementId *big.Int, lastReporter common.Address, startBlock *big.Int) (bool, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "validateNonce", nonce, merkleRoot, reporter, settlementId, lastReporter, startBlock)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// ValidateNonce is a free data retrieval call binding the contract method 0x0ad50f40.
//
// Solidity: function validateNonce(uint256 nonce, bytes32 merkleRoot, address reporter, uint256 settlementId, address lastReporter, uint256 startBlock) view returns(bool)
func (_Consensus *ConsensusSession) ValidateNonce(nonce *big.Int, merkleRoot [32]byte, reporter common.Address, settlementId *big.Int, lastReporter common.Address, startBlock *big.Int) (bool, error) {
	return _Consensus.Contract.ValidateNonce(&_Consensus.CallOpts, nonce, merkleRoot, reporter, settlementId, lastReporter, startBlock)
}

// ValidateNonce is a free data retrieval call binding the contract method 0x0ad50f40.
//
// Solidity: function validateNonce(uint256 nonce, bytes32 merkleRoot, address reporter, uint256 settlementId, address lastReporter, uint256 startBlock) view returns(bool)
func (_Consensus *ConsensusCallerSession) ValidateNonce(nonce *big.Int, merkleRoot [32]byte, reporter common.Address, settlementId *big.Int, lastReporter common.Address, startBlock *big.Int) (bool, error) {
	return _Consensus.Contract.ValidateNonce(&_Consensus.CallOpts, nonce, merkleRoot, reporter, settlementId, lastReporter, startBlock)
}

// ValidateWork is a free data retrieval call binding the contract method 0xd31ca1a8.
//
// Solidity: function validateWork(bytes32 work, uint256 difficulty) view returns(bool)
func (_Consensus *ConsensusCaller) ValidateWork(opts *bind.CallOpts, work [32]byte, difficulty *big.Int) (bool, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "validateWork", work, difficulty)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// ValidateWork is a free data retrieval call binding the contract method 0xd31ca1a8.
//
// Solidity: function validateWork(bytes32 work, uint256 difficulty) view returns(bool)
func (_Consensus *ConsensusSession) ValidateWork(work [32]byte, difficulty *big.Int) (bool, error) {
	return _Consensus.Contract.ValidateWork(&_Consensus.CallOpts, work, difficulty)
}

// ValidateWork is a free data retrieval call binding the contract method 0xd31ca1a8.
//
// Solidity: function validateWork(bytes32 work, uint256 difficulty) view returns(bool)
func (_Consensus *ConsensusCallerSession) ValidateWork(work [32]byte, difficulty *big.Int) (bool, error) {
	return _Consensus.Contract.ValidateWork(&_Consensus.CallOpts, work, difficulty)
}

// VerifyMerkleTree is a free data retrieval call binding the contract method 0x449ee222.
//
// Solidity: function verifyMerkleTree((uint256,address,address,address,bool)[] obligations) pure returns(bytes32)
func (_Consensus *ConsensusCaller) VerifyMerkleTree(opts *bind.CallOpts, obligations []SettlementLibObligation) ([32]byte, error) {
	var out []interface{}
	err := _Consensus.contract.Call(opts, &out, "verifyMerkleTree", obligations)

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// VerifyMerkleTree is a free data retrieval call binding the contract method 0x449ee222.
//
// Solidity: function verifyMerkleTree((uint256,address,address,address,bool)[] obligations) pure returns(bytes32)
func (_Consensus *ConsensusSession) VerifyMerkleTree(obligations []SettlementLibObligation) ([32]byte, error) {
	return _Consensus.Contract.VerifyMerkleTree(&_Consensus.CallOpts, obligations)
}

// VerifyMerkleTree is a free data retrieval call binding the contract method 0x449ee222.
//
// Solidity: function verifyMerkleTree((uint256,address,address,address,bool)[] obligations) pure returns(bytes32)
func (_Consensus *ConsensusCallerSession) VerifyMerkleTree(obligations []SettlementLibObligation) ([32]byte, error) {
	return _Consensus.Contract.VerifyMerkleTree(&_Consensus.CallOpts, obligations)
}

// ReportExchangeMerkleRootConcur is a paid mutator transaction binding the contract method 0x6779dd00.
//
// Solidity: function reportExchangeMerkleRootConcur((bytes32,uint256,address) exchangeReport, uint8 v, bytes32 r, bytes32 s) returns()
func (_Consensus *ConsensusTransactor) ReportExchangeMerkleRootConcur(opts *bind.TransactOpts, exchangeReport SettlementDataConsensusExchangeReport, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _Consensus.contract.Transact(opts, "reportExchangeMerkleRootConcur", exchangeReport, v, r, s)
}

// ReportExchangeMerkleRootConcur is a paid mutator transaction binding the contract method 0x6779dd00.
//
// Solidity: function reportExchangeMerkleRootConcur((bytes32,uint256,address) exchangeReport, uint8 v, bytes32 r, bytes32 s) returns()
func (_Consensus *ConsensusSession) ReportExchangeMerkleRootConcur(exchangeReport SettlementDataConsensusExchangeReport, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _Consensus.Contract.ReportExchangeMerkleRootConcur(&_Consensus.TransactOpts, exchangeReport, v, r, s)
}

// ReportExchangeMerkleRootConcur is a paid mutator transaction binding the contract method 0x6779dd00.
//
// Solidity: function reportExchangeMerkleRootConcur((bytes32,uint256,address) exchangeReport, uint8 v, bytes32 r, bytes32 s) returns()
func (_Consensus *ConsensusTransactorSession) ReportExchangeMerkleRootConcur(exchangeReport SettlementDataConsensusExchangeReport, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _Consensus.Contract.ReportExchangeMerkleRootConcur(&_Consensus.TransactOpts, exchangeReport, v, r, s)
}

// ReportExchangeMerkleRootDispute is a paid mutator transaction binding the contract method 0x5fce4316.
//
// Solidity: function reportExchangeMerkleRootDispute((uint256,address,address,address,bool)[] reportedObligations, (bytes32,uint256,address) exchangeReport, uint8 v, bytes32 r, bytes32 s) returns()
func (_Consensus *ConsensusTransactor) ReportExchangeMerkleRootDispute(opts *bind.TransactOpts, reportedObligations []SettlementLibObligation, exchangeReport SettlementDataConsensusExchangeReport, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _Consensus.contract.Transact(opts, "reportExchangeMerkleRootDispute", reportedObligations, exchangeReport, v, r, s)
}

// ReportExchangeMerkleRootDispute is a paid mutator transaction binding the contract method 0x5fce4316.
//
// Solidity: function reportExchangeMerkleRootDispute((uint256,address,address,address,bool)[] reportedObligations, (bytes32,uint256,address) exchangeReport, uint8 v, bytes32 r, bytes32 s) returns()
func (_Consensus *ConsensusSession) ReportExchangeMerkleRootDispute(reportedObligations []SettlementLibObligation, exchangeReport SettlementDataConsensusExchangeReport, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _Consensus.Contract.ReportExchangeMerkleRootDispute(&_Consensus.TransactOpts, reportedObligations, exchangeReport, v, r, s)
}

// ReportExchangeMerkleRootDispute is a paid mutator transaction binding the contract method 0x5fce4316.
//
// Solidity: function reportExchangeMerkleRootDispute((uint256,address,address,address,bool)[] reportedObligations, (bytes32,uint256,address) exchangeReport, uint8 v, bytes32 r, bytes32 s) returns()
func (_Consensus *ConsensusTransactorSession) ReportExchangeMerkleRootDispute(reportedObligations []SettlementLibObligation, exchangeReport SettlementDataConsensusExchangeReport, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _Consensus.Contract.ReportExchangeMerkleRootDispute(&_Consensus.TransactOpts, reportedObligations, exchangeReport, v, r, s)
}

// ReportMerkleRootQuorumDisputed is a paid mutator transaction binding the contract method 0x135f3c49.
//
// Solidity: function reportMerkleRootQuorumDisputed((uint256,address,address,address,bool)[] reportedObligations, (bytes32,uint256,address) exchangeReport, uint8 v, bytes32 r, bytes32 s) returns()
func (_Consensus *ConsensusTransactor) ReportMerkleRootQuorumDisputed(opts *bind.TransactOpts, reportedObligations []SettlementLibObligation, exchangeReport SettlementDataConsensusExchangeReport, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _Consensus.contract.Transact(opts, "reportMerkleRootQuorumDisputed", reportedObligations, exchangeReport, v, r, s)
}

// ReportMerkleRootQuorumDisputed is a paid mutator transaction binding the contract method 0x135f3c49.
//
// Solidity: function reportMerkleRootQuorumDisputed((uint256,address,address,address,bool)[] reportedObligations, (bytes32,uint256,address) exchangeReport, uint8 v, bytes32 r, bytes32 s) returns()
func (_Consensus *ConsensusSession) ReportMerkleRootQuorumDisputed(reportedObligations []SettlementLibObligation, exchangeReport SettlementDataConsensusExchangeReport, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _Consensus.Contract.ReportMerkleRootQuorumDisputed(&_Consensus.TransactOpts, reportedObligations, exchangeReport, v, r, s)
}

// ReportMerkleRootQuorumDisputed is a paid mutator transaction binding the contract method 0x135f3c49.
//
// Solidity: function reportMerkleRootQuorumDisputed((uint256,address,address,address,bool)[] reportedObligations, (bytes32,uint256,address) exchangeReport, uint8 v, bytes32 r, bytes32 s) returns()
func (_Consensus *ConsensusTransactorSession) ReportMerkleRootQuorumDisputed(reportedObligations []SettlementLibObligation, exchangeReport SettlementDataConsensusExchangeReport, v uint8, r [32]byte, s [32]byte) (*types.Transaction, error) {
	return _Consensus.Contract.ReportMerkleRootQuorumDisputed(&_Consensus.TransactOpts, reportedObligations, exchangeReport, v, r, s)
}

// ReportSettlementObligations is a paid mutator transaction binding the contract method 0x4eadc688.
//
// Solidity: function reportSettlementObligations(address coordinator, uint256 settlementId, (uint256,address,address,address,bool)[] reportedObligations, bytes32 merkleRoot, uint256 nonce) returns()
func (_Consensus *ConsensusTransactor) ReportSettlementObligations(opts *bind.TransactOpts, coordinator common.Address, settlementId *big.Int, reportedObligations []SettlementLibObligation, merkleRoot [32]byte, nonce *big.Int) (*types.Transaction, error) {
	return _Consensus.contract.Transact(opts, "reportSettlementObligations", coordinator, settlementId, reportedObligations, merkleRoot, nonce)
}

// ReportSettlementObligations is a paid mutator transaction binding the contract method 0x4eadc688.
//
// Solidity: function reportSettlementObligations(address coordinator, uint256 settlementId, (uint256,address,address,address,bool)[] reportedObligations, bytes32 merkleRoot, uint256 nonce) returns()
func (_Consensus *ConsensusSession) ReportSettlementObligations(coordinator common.Address, settlementId *big.Int, reportedObligations []SettlementLibObligation, merkleRoot [32]byte, nonce *big.Int) (*types.Transaction, error) {
	return _Consensus.Contract.ReportSettlementObligations(&_Consensus.TransactOpts, coordinator, settlementId, reportedObligations, merkleRoot, nonce)
}

// ReportSettlementObligations is a paid mutator transaction binding the contract method 0x4eadc688.
//
// Solidity: function reportSettlementObligations(address coordinator, uint256 settlementId, (uint256,address,address,address,bool)[] reportedObligations, bytes32 merkleRoot, uint256 nonce) returns()
func (_Consensus *ConsensusTransactorSession) ReportSettlementObligations(coordinator common.Address, settlementId *big.Int, reportedObligations []SettlementLibObligation, merkleRoot [32]byte, nonce *big.Int) (*types.Transaction, error) {
	return _Consensus.Contract.ReportSettlementObligations(&_Consensus.TransactOpts, coordinator, settlementId, reportedObligations, merkleRoot, nonce)
}

// ConsensusObligationsReportedIterator is returned from FilterObligationsReported and is used to iterate over the raw logs and unpacked data for ObligationsReported events raised by the Consensus contract.
type ConsensusObligationsReportedIterator struct {
	Event *ConsensusObligationsReported // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *ConsensusObligationsReportedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(ConsensusObligationsReported)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(ConsensusObligationsReported)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *ConsensusObligationsReportedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *ConsensusObligationsReportedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// ConsensusObligationsReported represents a ObligationsReported event raised by the Consensus contract.
type ConsensusObligationsReported struct {
	Coordinator common.Address
	Id          *big.Int
	Sdp         common.Address
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterObligationsReported is a free log retrieval operation binding the contract event 0xb80bead4658293cf280b91c72809a345788c010b062243c3b7823fa1c9373112.
//
// Solidity: event ObligationsReported(address coordinator, uint256 id, address sdp)
func (_Consensus *ConsensusFilterer) FilterObligationsReported(opts *bind.FilterOpts) (*ConsensusObligationsReportedIterator, error) {

	logs, sub, err := _Consensus.contract.FilterLogs(opts, "ObligationsReported")
	if err != nil {
		return nil, err
	}
	return &ConsensusObligationsReportedIterator{contract: _Consensus.contract, event: "ObligationsReported", logs: logs, sub: sub}, nil
}

// WatchObligationsReported is a free log subscription operation binding the contract event 0xb80bead4658293cf280b91c72809a345788c010b062243c3b7823fa1c9373112.
//
// Solidity: event ObligationsReported(address coordinator, uint256 id, address sdp)
func (_Consensus *ConsensusFilterer) WatchObligationsReported(opts *bind.WatchOpts, sink chan<- *ConsensusObligationsReported) (event.Subscription, error) {

	logs, sub, err := _Consensus.contract.WatchLogs(opts, "ObligationsReported")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(ConsensusObligationsReported)
				if err := _Consensus.contract.UnpackLog(event, "ObligationsReported", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseObligationsReported is a log parse operation binding the contract event 0xb80bead4658293cf280b91c72809a345788c010b062243c3b7823fa1c9373112.
//
// Solidity: event ObligationsReported(address coordinator, uint256 id, address sdp)
func (_Consensus *ConsensusFilterer) ParseObligationsReported(log types.Log) (*ConsensusObligationsReported, error) {
	event := new(ConsensusObligationsReported)
	if err := _Consensus.contract.UnpackLog(event, "ObligationsReported", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// ConsensusQuorumDisputeSettledExchangeIterator is returned from FilterQuorumDisputeSettledExchange and is used to iterate over the raw logs and unpacked data for QuorumDisputeSettledExchange events raised by the Consensus contract.
type ConsensusQuorumDisputeSettledExchangeIterator struct {
	Event *ConsensusQuorumDisputeSettledExchange // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *ConsensusQuorumDisputeSettledExchangeIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(ConsensusQuorumDisputeSettledExchange)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(ConsensusQuorumDisputeSettledExchange)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *ConsensusQuorumDisputeSettledExchangeIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *ConsensusQuorumDisputeSettledExchangeIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// ConsensusQuorumDisputeSettledExchange represents a QuorumDisputeSettledExchange event raised by the Consensus contract.
type ConsensusQuorumDisputeSettledExchange struct {
	SettlementId *big.Int
	CorrectRoot  [32]byte
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterQuorumDisputeSettledExchange is a free log retrieval operation binding the contract event 0xffc3d50d019df81122997312bd34ebf914c382b2e8510c473109a468f22ee695.
//
// Solidity: event QuorumDisputeSettledExchange(uint256 settlementId, bytes32 correctRoot)
func (_Consensus *ConsensusFilterer) FilterQuorumDisputeSettledExchange(opts *bind.FilterOpts) (*ConsensusQuorumDisputeSettledExchangeIterator, error) {

	logs, sub, err := _Consensus.contract.FilterLogs(opts, "QuorumDisputeSettledExchange")
	if err != nil {
		return nil, err
	}
	return &ConsensusQuorumDisputeSettledExchangeIterator{contract: _Consensus.contract, event: "QuorumDisputeSettledExchange", logs: logs, sub: sub}, nil
}

// WatchQuorumDisputeSettledExchange is a free log subscription operation binding the contract event 0xffc3d50d019df81122997312bd34ebf914c382b2e8510c473109a468f22ee695.
//
// Solidity: event QuorumDisputeSettledExchange(uint256 settlementId, bytes32 correctRoot)
func (_Consensus *ConsensusFilterer) WatchQuorumDisputeSettledExchange(opts *bind.WatchOpts, sink chan<- *ConsensusQuorumDisputeSettledExchange) (event.Subscription, error) {

	logs, sub, err := _Consensus.contract.WatchLogs(opts, "QuorumDisputeSettledExchange")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(ConsensusQuorumDisputeSettledExchange)
				if err := _Consensus.contract.UnpackLog(event, "QuorumDisputeSettledExchange", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseQuorumDisputeSettledExchange is a log parse operation binding the contract event 0xffc3d50d019df81122997312bd34ebf914c382b2e8510c473109a468f22ee695.
//
// Solidity: event QuorumDisputeSettledExchange(uint256 settlementId, bytes32 correctRoot)
func (_Consensus *ConsensusFilterer) ParseQuorumDisputeSettledExchange(log types.Log) (*ConsensusQuorumDisputeSettledExchange, error) {
	event := new(ConsensusQuorumDisputeSettledExchange)
	if err := _Consensus.contract.UnpackLog(event, "QuorumDisputeSettledExchange", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// ConsensusSettlementConcurredExchangeIterator is returned from FilterSettlementConcurredExchange and is used to iterate over the raw logs and unpacked data for SettlementConcurredExchange events raised by the Consensus contract.
type ConsensusSettlementConcurredExchangeIterator struct {
	Event *ConsensusSettlementConcurredExchange // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *ConsensusSettlementConcurredExchangeIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(ConsensusSettlementConcurredExchange)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(ConsensusSettlementConcurredExchange)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *ConsensusSettlementConcurredExchangeIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *ConsensusSettlementConcurredExchangeIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// ConsensusSettlementConcurredExchange represents a SettlementConcurredExchange event raised by the Consensus contract.
type ConsensusSettlementConcurredExchange struct {
	SettlementId *big.Int
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterSettlementConcurredExchange is a free log retrieval operation binding the contract event 0xfc805df595f93d1259cc1fc0ee19c4f5f35efca164f1d7fc2aed7cdf0406aeec.
//
// Solidity: event SettlementConcurredExchange(uint256 settlementId)
func (_Consensus *ConsensusFilterer) FilterSettlementConcurredExchange(opts *bind.FilterOpts) (*ConsensusSettlementConcurredExchangeIterator, error) {

	logs, sub, err := _Consensus.contract.FilterLogs(opts, "SettlementConcurredExchange")
	if err != nil {
		return nil, err
	}
	return &ConsensusSettlementConcurredExchangeIterator{contract: _Consensus.contract, event: "SettlementConcurredExchange", logs: logs, sub: sub}, nil
}

// WatchSettlementConcurredExchange is a free log subscription operation binding the contract event 0xfc805df595f93d1259cc1fc0ee19c4f5f35efca164f1d7fc2aed7cdf0406aeec.
//
// Solidity: event SettlementConcurredExchange(uint256 settlementId)
func (_Consensus *ConsensusFilterer) WatchSettlementConcurredExchange(opts *bind.WatchOpts, sink chan<- *ConsensusSettlementConcurredExchange) (event.Subscription, error) {

	logs, sub, err := _Consensus.contract.WatchLogs(opts, "SettlementConcurredExchange")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(ConsensusSettlementConcurredExchange)
				if err := _Consensus.contract.UnpackLog(event, "SettlementConcurredExchange", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseSettlementConcurredExchange is a log parse operation binding the contract event 0xfc805df595f93d1259cc1fc0ee19c4f5f35efca164f1d7fc2aed7cdf0406aeec.
//
// Solidity: event SettlementConcurredExchange(uint256 settlementId)
func (_Consensus *ConsensusFilterer) ParseSettlementConcurredExchange(log types.Log) (*ConsensusSettlementConcurredExchange, error) {
	event := new(ConsensusSettlementConcurredExchange)
	if err := _Consensus.contract.UnpackLog(event, "SettlementConcurredExchange", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// ConsensusSettlementDisputedExchangeIterator is returned from FilterSettlementDisputedExchange and is used to iterate over the raw logs and unpacked data for SettlementDisputedExchange events raised by the Consensus contract.
type ConsensusSettlementDisputedExchangeIterator struct {
	Event *ConsensusSettlementDisputedExchange // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *ConsensusSettlementDisputedExchangeIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(ConsensusSettlementDisputedExchange)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(ConsensusSettlementDisputedExchange)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *ConsensusSettlementDisputedExchangeIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *ConsensusSettlementDisputedExchangeIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// ConsensusSettlementDisputedExchange represents a SettlementDisputedExchange event raised by the Consensus contract.
type ConsensusSettlementDisputedExchange struct {
	SettlementId *big.Int
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterSettlementDisputedExchange is a free log retrieval operation binding the contract event 0xb3dca7a92a457725c7350605909db6bc3bff0628c36bb3caa742d01928e61d8c.
//
// Solidity: event SettlementDisputedExchange(uint256 settlementId)
func (_Consensus *ConsensusFilterer) FilterSettlementDisputedExchange(opts *bind.FilterOpts) (*ConsensusSettlementDisputedExchangeIterator, error) {

	logs, sub, err := _Consensus.contract.FilterLogs(opts, "SettlementDisputedExchange")
	if err != nil {
		return nil, err
	}
	return &ConsensusSettlementDisputedExchangeIterator{contract: _Consensus.contract, event: "SettlementDisputedExchange", logs: logs, sub: sub}, nil
}

// WatchSettlementDisputedExchange is a free log subscription operation binding the contract event 0xb3dca7a92a457725c7350605909db6bc3bff0628c36bb3caa742d01928e61d8c.
//
// Solidity: event SettlementDisputedExchange(uint256 settlementId)
func (_Consensus *ConsensusFilterer) WatchSettlementDisputedExchange(opts *bind.WatchOpts, sink chan<- *ConsensusSettlementDisputedExchange) (event.Subscription, error) {

	logs, sub, err := _Consensus.contract.WatchLogs(opts, "SettlementDisputedExchange")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(ConsensusSettlementDisputedExchange)
				if err := _Consensus.contract.UnpackLog(event, "SettlementDisputedExchange", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseSettlementDisputedExchange is a log parse operation binding the contract event 0xb3dca7a92a457725c7350605909db6bc3bff0628c36bb3caa742d01928e61d8c.
//
// Solidity: event SettlementDisputedExchange(uint256 settlementId)
func (_Consensus *ConsensusFilterer) ParseSettlementDisputedExchange(log types.Log) (*ConsensusSettlementDisputedExchange, error) {
	event := new(ConsensusSettlementDisputedExchange)
	if err := _Consensus.contract.UnpackLog(event, "SettlementDisputedExchange", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// ConsensusSettlementDisputedQuorumIterator is returned from FilterSettlementDisputedQuorum and is used to iterate over the raw logs and unpacked data for SettlementDisputedQuorum events raised by the Consensus contract.
type ConsensusSettlementDisputedQuorumIterator struct {
	Event *ConsensusSettlementDisputedQuorum // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *ConsensusSettlementDisputedQuorumIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(ConsensusSettlementDisputedQuorum)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(ConsensusSettlementDisputedQuorum)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *ConsensusSettlementDisputedQuorumIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *ConsensusSettlementDisputedQuorumIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// ConsensusSettlementDisputedQuorum represents a SettlementDisputedQuorum event raised by the Consensus contract.
type ConsensusSettlementDisputedQuorum struct {
	SettlementId *big.Int
	Raw          types.Log // Blockchain specific contextual infos
}

// FilterSettlementDisputedQuorum is a free log retrieval operation binding the contract event 0x7dc5ee54bd440b1555c6e1e15cb9ac0a6169771de4897b8f3f2d26ad0539538a.
//
// Solidity: event SettlementDisputedQuorum(uint256 settlementId)
func (_Consensus *ConsensusFilterer) FilterSettlementDisputedQuorum(opts *bind.FilterOpts) (*ConsensusSettlementDisputedQuorumIterator, error) {

	logs, sub, err := _Consensus.contract.FilterLogs(opts, "SettlementDisputedQuorum")
	if err != nil {
		return nil, err
	}
	return &ConsensusSettlementDisputedQuorumIterator{contract: _Consensus.contract, event: "SettlementDisputedQuorum", logs: logs, sub: sub}, nil
}

// WatchSettlementDisputedQuorum is a free log subscription operation binding the contract event 0x7dc5ee54bd440b1555c6e1e15cb9ac0a6169771de4897b8f3f2d26ad0539538a.
//
// Solidity: event SettlementDisputedQuorum(uint256 settlementId)
func (_Consensus *ConsensusFilterer) WatchSettlementDisputedQuorum(opts *bind.WatchOpts, sink chan<- *ConsensusSettlementDisputedQuorum) (event.Subscription, error) {

	logs, sub, err := _Consensus.contract.WatchLogs(opts, "SettlementDisputedQuorum")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(ConsensusSettlementDisputedQuorum)
				if err := _Consensus.contract.UnpackLog(event, "SettlementDisputedQuorum", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseSettlementDisputedQuorum is a log parse operation binding the contract event 0x7dc5ee54bd440b1555c6e1e15cb9ac0a6169771de4897b8f3f2d26ad0539538a.
//
// Solidity: event SettlementDisputedQuorum(uint256 settlementId)
func (_Consensus *ConsensusFilterer) ParseSettlementDisputedQuorum(log types.Log) (*ConsensusSettlementDisputedQuorum, error) {
	event := new(ConsensusSettlementDisputedQuorum)
	if err := _Consensus.contract.UnpackLog(event, "SettlementDisputedQuorum", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
