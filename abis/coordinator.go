// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package abis

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// CoordinationInterfaceSettlementData is an auto generated low-level Go binding around an user-defined struct.
type CoordinationInterfaceSettlementData struct {
	RequestBlock *big.Int
	Token        common.Address
	Wallet       common.Address
}

// // SettlementLibObligation is an auto generated low-level Go binding around an user-defined struct.
// type SettlementLibObligation struct {
// 	Amount     *big.Int
// 	Deliverer  common.Address
// 	Recipient  common.Address
// 	Token      common.Address
// 	Reallocate bool
// }

// CoordinatorMetaData contains all meta data concerning the Coordinator contract.
var CoordinatorMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_identity\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"requester\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"cleared\",\"type\":\"uint256\"}],\"name\":\"ObligationsWritten\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"reqId\",\"type\":\"uint256\"}],\"name\":\"getReqData\",\"outputs\":[{\"components\":[{\"internalType\":\"uint256\",\"name\":\"requestBlock\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"wallet\",\"type\":\"address\"}],\"internalType\":\"structCoordinationInterface.SettlementData\",\"name\":\"\",\"type\":\"tuple\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"lastSettlementIdProcessed\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"nextRequestId\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"components\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"deliverer\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"reallocate\",\"type\":\"bool\"}],\"internalType\":\"structSettlementLib.Obligation[]\",\"name\":\"obligations\",\"type\":\"tuple[]\"}],\"name\":\"reportObligations\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"trader\",\"type\":\"address\"}],\"name\":\"requestSettlement\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// CoordinatorABI is the input ABI used to generate the binding from.
// Deprecated: Use CoordinatorMetaData.ABI instead.
var CoordinatorABI = CoordinatorMetaData.ABI

// Coordinator is an auto generated Go binding around an Ethereum contract.
type Coordinator struct {
	CoordinatorCaller     // Read-only binding to the contract
	CoordinatorTransactor // Write-only binding to the contract
	CoordinatorFilterer   // Log filterer for contract events
}

// CoordinatorCaller is an auto generated read-only Go binding around an Ethereum contract.
type CoordinatorCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// CoordinatorTransactor is an auto generated write-only Go binding around an Ethereum contract.
type CoordinatorTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// CoordinatorFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type CoordinatorFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// CoordinatorSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type CoordinatorSession struct {
	Contract     *Coordinator      // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// CoordinatorCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type CoordinatorCallerSession struct {
	Contract *CoordinatorCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts      // Call options to use throughout this session
}

// CoordinatorTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type CoordinatorTransactorSession struct {
	Contract     *CoordinatorTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts      // Transaction auth options to use throughout this session
}

// CoordinatorRaw is an auto generated low-level Go binding around an Ethereum contract.
type CoordinatorRaw struct {
	Contract *Coordinator // Generic contract binding to access the raw methods on
}

// CoordinatorCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type CoordinatorCallerRaw struct {
	Contract *CoordinatorCaller // Generic read-only contract binding to access the raw methods on
}

// CoordinatorTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type CoordinatorTransactorRaw struct {
	Contract *CoordinatorTransactor // Generic write-only contract binding to access the raw methods on
}

// NewCoordinator creates a new instance of Coordinator, bound to a specific deployed contract.
func NewCoordinator(address common.Address, backend bind.ContractBackend) (*Coordinator, error) {
	contract, err := bindCoordinator(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Coordinator{CoordinatorCaller: CoordinatorCaller{contract: contract}, CoordinatorTransactor: CoordinatorTransactor{contract: contract}, CoordinatorFilterer: CoordinatorFilterer{contract: contract}}, nil
}

// NewCoordinatorCaller creates a new read-only instance of Coordinator, bound to a specific deployed contract.
func NewCoordinatorCaller(address common.Address, caller bind.ContractCaller) (*CoordinatorCaller, error) {
	contract, err := bindCoordinator(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &CoordinatorCaller{contract: contract}, nil
}

// NewCoordinatorTransactor creates a new write-only instance of Coordinator, bound to a specific deployed contract.
func NewCoordinatorTransactor(address common.Address, transactor bind.ContractTransactor) (*CoordinatorTransactor, error) {
	contract, err := bindCoordinator(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &CoordinatorTransactor{contract: contract}, nil
}

// NewCoordinatorFilterer creates a new log filterer instance of Coordinator, bound to a specific deployed contract.
func NewCoordinatorFilterer(address common.Address, filterer bind.ContractFilterer) (*CoordinatorFilterer, error) {
	contract, err := bindCoordinator(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &CoordinatorFilterer{contract: contract}, nil
}

// bindCoordinator binds a generic wrapper to an already deployed contract.
func bindCoordinator(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(CoordinatorABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Coordinator *CoordinatorRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Coordinator.Contract.CoordinatorCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Coordinator *CoordinatorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Coordinator.Contract.CoordinatorTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Coordinator *CoordinatorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Coordinator.Contract.CoordinatorTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Coordinator *CoordinatorCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Coordinator.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Coordinator *CoordinatorTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Coordinator.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Coordinator *CoordinatorTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Coordinator.Contract.contract.Transact(opts, method, params...)
}

// GetReqData is a free data retrieval call binding the contract method 0x28d3e3b5.
//
// Solidity: function getReqData(uint256 reqId) view returns((uint256,address,address))
func (_Coordinator *CoordinatorCaller) GetReqData(opts *bind.CallOpts, reqId *big.Int) (CoordinationInterfaceSettlementData, error) {
	var out []interface{}
	err := _Coordinator.contract.Call(opts, &out, "getReqData", reqId)

	if err != nil {
		return *new(CoordinationInterfaceSettlementData), err
	}

	out0 := *abi.ConvertType(out[0], new(CoordinationInterfaceSettlementData)).(*CoordinationInterfaceSettlementData)

	return out0, err

}

// GetReqData is a free data retrieval call binding the contract method 0x28d3e3b5.
//
// Solidity: function getReqData(uint256 reqId) view returns((uint256,address,address))
func (_Coordinator *CoordinatorSession) GetReqData(reqId *big.Int) (CoordinationInterfaceSettlementData, error) {
	return _Coordinator.Contract.GetReqData(&_Coordinator.CallOpts, reqId)
}

// GetReqData is a free data retrieval call binding the contract method 0x28d3e3b5.
//
// Solidity: function getReqData(uint256 reqId) view returns((uint256,address,address))
func (_Coordinator *CoordinatorCallerSession) GetReqData(reqId *big.Int) (CoordinationInterfaceSettlementData, error) {
	return _Coordinator.Contract.GetReqData(&_Coordinator.CallOpts, reqId)
}

// LastSettlementIdProcessed is a free data retrieval call binding the contract method 0x4698bb22.
//
// Solidity: function lastSettlementIdProcessed() view returns(uint256)
func (_Coordinator *CoordinatorCaller) LastSettlementIdProcessed(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Coordinator.contract.Call(opts, &out, "lastSettlementIdProcessed")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// LastSettlementIdProcessed is a free data retrieval call binding the contract method 0x4698bb22.
//
// Solidity: function lastSettlementIdProcessed() view returns(uint256)
func (_Coordinator *CoordinatorSession) LastSettlementIdProcessed() (*big.Int, error) {
	return _Coordinator.Contract.LastSettlementIdProcessed(&_Coordinator.CallOpts)
}

// LastSettlementIdProcessed is a free data retrieval call binding the contract method 0x4698bb22.
//
// Solidity: function lastSettlementIdProcessed() view returns(uint256)
func (_Coordinator *CoordinatorCallerSession) LastSettlementIdProcessed() (*big.Int, error) {
	return _Coordinator.Contract.LastSettlementIdProcessed(&_Coordinator.CallOpts)
}

// NextRequestId is a free data retrieval call binding the contract method 0x6a84a985.
//
// Solidity: function nextRequestId() view returns(uint256)
func (_Coordinator *CoordinatorCaller) NextRequestId(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Coordinator.contract.Call(opts, &out, "nextRequestId")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// NextRequestId is a free data retrieval call binding the contract method 0x6a84a985.
//
// Solidity: function nextRequestId() view returns(uint256)
func (_Coordinator *CoordinatorSession) NextRequestId() (*big.Int, error) {
	return _Coordinator.Contract.NextRequestId(&_Coordinator.CallOpts)
}

// NextRequestId is a free data retrieval call binding the contract method 0x6a84a985.
//
// Solidity: function nextRequestId() view returns(uint256)
func (_Coordinator *CoordinatorCallerSession) NextRequestId() (*big.Int, error) {
	return _Coordinator.Contract.NextRequestId(&_Coordinator.CallOpts)
}

// ReportObligations is a paid mutator transaction binding the contract method 0x66673443.
//
// Solidity: function reportObligations(uint256 id, (uint256,address,address,address,bool)[] obligations) returns()
func (_Coordinator *CoordinatorTransactor) ReportObligations(opts *bind.TransactOpts, id *big.Int, obligations []SettlementLibObligation) (*types.Transaction, error) {
	return _Coordinator.contract.Transact(opts, "reportObligations", id, obligations)
}

// ReportObligations is a paid mutator transaction binding the contract method 0x66673443.
//
// Solidity: function reportObligations(uint256 id, (uint256,address,address,address,bool)[] obligations) returns()
func (_Coordinator *CoordinatorSession) ReportObligations(id *big.Int, obligations []SettlementLibObligation) (*types.Transaction, error) {
	return _Coordinator.Contract.ReportObligations(&_Coordinator.TransactOpts, id, obligations)
}

// ReportObligations is a paid mutator transaction binding the contract method 0x66673443.
//
// Solidity: function reportObligations(uint256 id, (uint256,address,address,address,bool)[] obligations) returns()
func (_Coordinator *CoordinatorTransactorSession) ReportObligations(id *big.Int, obligations []SettlementLibObligation) (*types.Transaction, error) {
	return _Coordinator.Contract.ReportObligations(&_Coordinator.TransactOpts, id, obligations)
}

// RequestSettlement is a paid mutator transaction binding the contract method 0x3003f147.
//
// Solidity: function requestSettlement(address token, address trader) returns(uint256)
func (_Coordinator *CoordinatorTransactor) RequestSettlement(opts *bind.TransactOpts, token common.Address, trader common.Address) (*types.Transaction, error) {
	return _Coordinator.contract.Transact(opts, "requestSettlement", token, trader)
}

// RequestSettlement is a paid mutator transaction binding the contract method 0x3003f147.
//
// Solidity: function requestSettlement(address token, address trader) returns(uint256)
func (_Coordinator *CoordinatorSession) RequestSettlement(token common.Address, trader common.Address) (*types.Transaction, error) {
	return _Coordinator.Contract.RequestSettlement(&_Coordinator.TransactOpts, token, trader)
}

// RequestSettlement is a paid mutator transaction binding the contract method 0x3003f147.
//
// Solidity: function requestSettlement(address token, address trader) returns(uint256)
func (_Coordinator *CoordinatorTransactorSession) RequestSettlement(token common.Address, trader common.Address) (*types.Transaction, error) {
	return _Coordinator.Contract.RequestSettlement(&_Coordinator.TransactOpts, token, trader)
}

// CoordinatorObligationsWrittenIterator is returned from FilterObligationsWritten and is used to iterate over the raw logs and unpacked data for ObligationsWritten events raised by the Coordinator contract.
type CoordinatorObligationsWrittenIterator struct {
	Event *CoordinatorObligationsWritten // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *CoordinatorObligationsWrittenIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(CoordinatorObligationsWritten)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(CoordinatorObligationsWritten)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *CoordinatorObligationsWrittenIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *CoordinatorObligationsWrittenIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// CoordinatorObligationsWritten represents a ObligationsWritten event raised by the Coordinator contract.
type CoordinatorObligationsWritten struct {
	Id        *big.Int
	Requester common.Address
	Token     common.Address
	Cleared   *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterObligationsWritten is a free log retrieval operation binding the contract event 0x2fbe32bf5c43587f241785776e03af9725712c4d9349d5d487e0b080554759cf.
//
// Solidity: event ObligationsWritten(uint256 id, address requester, address token, uint256 cleared)
func (_Coordinator *CoordinatorFilterer) FilterObligationsWritten(opts *bind.FilterOpts) (*CoordinatorObligationsWrittenIterator, error) {

	logs, sub, err := _Coordinator.contract.FilterLogs(opts, "ObligationsWritten")
	if err != nil {
		return nil, err
	}
	return &CoordinatorObligationsWrittenIterator{contract: _Coordinator.contract, event: "ObligationsWritten", logs: logs, sub: sub}, nil
}

// WatchObligationsWritten is a free log subscription operation binding the contract event 0x2fbe32bf5c43587f241785776e03af9725712c4d9349d5d487e0b080554759cf.
//
// Solidity: event ObligationsWritten(uint256 id, address requester, address token, uint256 cleared)
func (_Coordinator *CoordinatorFilterer) WatchObligationsWritten(opts *bind.WatchOpts, sink chan<- *CoordinatorObligationsWritten) (event.Subscription, error) {

	logs, sub, err := _Coordinator.contract.WatchLogs(opts, "ObligationsWritten")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(CoordinatorObligationsWritten)
				if err := _Coordinator.contract.UnpackLog(event, "ObligationsWritten", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseObligationsWritten is a log parse operation binding the contract event 0x2fbe32bf5c43587f241785776e03af9725712c4d9349d5d487e0b080554759cf.
//
// Solidity: event ObligationsWritten(uint256 id, address requester, address token, uint256 cleared)
func (_Coordinator *CoordinatorFilterer) ParseObligationsWritten(log types.Log) (*CoordinatorObligationsWritten, error) {
	event := new(CoordinatorObligationsWritten)
	if err := _Coordinator.contract.UnpackLog(event, "ObligationsWritten", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
