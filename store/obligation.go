// Copyright © 2023 TXA PTE. LTD.

package store

import (
	"sdp/model"

	"gorm.io/gorm"
)

type ObligationStore struct {
	db *gorm.DB
}

func NewObligationStore(db *gorm.DB) *ObligationStore {
	return &ObligationStore{db: db}
}

func (s *ObligationStore) FindMany(bs *model.Obligation, filter *model.Obligation) ([]model.Obligation, error) {
	var m []model.Obligation
	if err := s.db.Where(&bs).Not(filter).Find(&m).Error; err != nil {
		return nil, err
	}
	return m, nil
}

func (s *ObligationStore) FindManyMap(a any, filter *model.Obligation) ([]model.Obligation, error) {
	var m []model.Obligation
	if err := s.db.Where(a).Not(filter).Find(&m).Error; err != nil {
		return nil, err
	}
	return m, nil
}

func (s *ObligationStore) FindOne(query interface{}) (*model.Obligation, error) {
	var m model.Obligation
	err := s.db.Where(query).First(&m).Error
	if err == gorm.ErrRecordNotFound {
		return nil, gorm.ErrRecordNotFound
	}
	if err != nil {
		return nil, err
	}
	return &m, nil
}
