# Copyright © 2023 TXA PTE. LTD.

# Builder
FROM golang:1.19-alpine@sha256:3183b343d01a6e8c9bee7b7c4eb7208518e68dc0cdac8623e1575820342472ed AS builder

WORKDIR /app

RUN apk update \
    && apk upgrade \
    && apk add git ca-certificates \
    && update-ca-certificates

RUN adduser \
    --disabled-password \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid 10000 \
    appuser

COPY go.* ./

RUN go mod download

COPY . ./

WORKDIR ./integration/detect_deposit

RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 \
    go build -ldflags "-s -w" \
    -buildvcs=false \
    -o /app/detect_deposit

WORKDIR ../process_trade

RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 \
    go build -ldflags "-s -w" \
    -buildvcs=false \
    -o /app/process_trade

WORKDIR /app

RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 \
    go build -ldflags "-s -w" \
    -buildvcs=false \
    -o /app/txa-sdp

# Final Image
FROM scratch AS final

# Integration tests
COPY --from=builder /app/process_trade /app/process_trade
COPY --from=builder /app/detect_deposit /app/detect_deposit

# Application
COPY --from=builder /app/txa-sdp /app/txa-sdp
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

EXPOSE 50051
EXPOSE 4000

USER appuser:appuser

ENTRYPOINT ["/app/txa-sdp"]
