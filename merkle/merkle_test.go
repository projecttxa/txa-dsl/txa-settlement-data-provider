// Copyright © 2023 TXA PTE. LTD.

package merkle

import (
	"testing"

	"sdp/abis"
	"sdp/constants"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/math"
)

var (
	deliverer    = "0xa98608103E8AA647bC5b980d018b011d91ab5DCF"
	recipient    = "0x981F0Bd6909901CaEAFc49177C229AE091bbD492"
	token        = constants.ZeroAddress
	counterAsset = "0x6b175474e89094c44da98b954eedeac495271d0f"
	amountString = "100000000000000000000"
)

func createObligation(t *testing.T, d, r, tt, a string) abis.SettlementLibObligation {
	t.Helper()

	return abis.SettlementLibObligation{
		Deliverer: common.HexToAddress(d),
		Recipient: common.HexToAddress(r),
		Token:     common.HexToAddress(tt),
		Amount:    math.MustParseBig256(a),
	}
}

func TestMerkleRootSingleObligation(t *testing.T) {
	// Initialize obligations
	o1 := createObligation(t, deliverer, recipient, token, amountString)
	o2 := createObligation(t, deliverer, recipient, counterAsset, amountString)

	obligations := []abis.SettlementLibObligation{o1, o2}

	// Generate a tree
	rootHash, proofs := GenerateMerkleTree(obligations)

	// Hash leaf nodes
	l1 := HashObligation(o1)
	l2 := HashObligation(o2)

	// prove that tree with rootHash contains obligation
	// will throw error and cause test to fail if proof is invalid
	if err := proofs[0].Verify(rootHash[:], l1); err != nil {
		t.Fatal(err)
	}
	if err := proofs[1].Verify(rootHash[:], l2); err != nil {
		t.Fatal(err)
	}
}
