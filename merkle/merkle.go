// Copyright © 2023 TXA PTE. LTD.

package merkle

import (
	"sdp/abis"

	solsha3 "github.com/miguelmota/go-solidity-sha3"
	"github.com/tendermint/tendermint/crypto/merkle"
)

// Given an array of obligations, hashes each one and constructs a merkle tree.
// Returns the tree as an array of hashes, the root hash, and an array of all membership proofs.
func GenerateMerkleTree(obligations []abis.SettlementLibObligation) ([32]byte, []*merkle.Proof) {
	// TODO: deterministically sort obligations
	// create bytes array same length as obligations array
	items := make([][]byte, len(obligations))
	// hash each obligation
	for i, obligation := range obligations {
		// add hash to bytes array
		items[i] = HashObligation(obligation)
	}

	// get root hash and proofs for each member of tree
	rootHash, proofs := merkle.ProofsFromByteSlices(items)
	return sizeMerkleRoot(rootHash), proofs
}

func HashObligation(obligation abis.SettlementLibObligation) []byte {
	types := []string{
		"address", // Deliverer
		"address", // Recipient
		"address", // TokenContract
		"uint256", // Amount
	}
	values := []interface{}{
		obligation.Deliverer.String(),
		obligation.Recipient.String(),
		obligation.Token.String(),
		obligation.Amount.String(),
	}
	// hash the obligation using same hashing function as solidity
	return solsha3.SoliditySHA3(
		types,
		values,
	)
}

func sizeMerkleRoot(root []byte) [32]byte {
	var res [32]byte
	copy(res[:], root)
	return res
}
