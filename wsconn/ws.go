// Copyright © 2023 TXA PTE. LTD.

package wsconn

import (
	"net/url"
	"sync"
	"time"

	"sdp/logger"
	"sdp/retry"

	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
)

type WS struct {
	mu     sync.Mutex
	Conn   *websocket.Conn
	host   string
	logger *logrus.Logger
}

func newWS(host string) *WS {
	var conn *websocket.Conn

	logger := logger.Default
	action := func(_ uint) error {
		var err error
		u := url.URL{Scheme: "ws", Host: host, Path: "/"}
		conn, err = getConn(u)
		if err != nil {
			logger.Error(err)
			return err
		}
		return nil
	}
	if err := retry.Do(action); err != nil {
		panic(err)
	}

	logger.Infof("ws connected to %v", host)
	return &WS{
		Conn:   conn,
		host:   host,
		logger: logger,
	}
}

func getConn(u url.URL) (*websocket.Conn, error) {
	conn, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		return nil, err
	}
	return conn, nil
}

func (w *WS) reconnect() error {
	var conn *websocket.Conn

	action := func(_ uint) error {
		var err error
		u := url.URL{Scheme: "ws", Host: w.host, Path: "/"}
		conn, err = getConn(u)
		if err != nil {
			return err
		}
		return nil
	}

	if err := retry.DoWithLimitAndFixedBackoff(action, 500, 4*time.Second); err != nil {
		return err
	}

	w.logger.Infof("ws reconnected to %v", w.host)
	w.mu.Lock()
	w.Conn = conn
	w.mu.Unlock()
	return nil
}

func (w *WS) PingAndReconnect() error {
	ticker := time.NewTicker(time.Second)
	for range ticker.C {
		if err := w.Conn.WriteControl(websocket.PingMessage, []byte{}, time.Now().Add(time.Second)); err != nil {
			if err := w.reconnect(); err != nil {
				w.logger.Errorf("reconnect failed: %v", err)
				return err
			}
		}
	}
	return nil
}
