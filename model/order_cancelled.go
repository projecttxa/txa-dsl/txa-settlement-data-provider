// Copyright © 2023 TXA PTE. LTD.

package model

import (
	"encoding/json"
)

type OrdersCanceledForSettlement struct {
	Address               string
	ChainId               int
	RequestSequenceNumber uint64
	SdpSequenceNumber     uint64 `gorm:"type:numeric;primaryKey;autoIncrement:false"`
	SequenceId            uint64
	SettlementId          string
	SystemSequenceNumber  uint64
	Timestamp             uint64
	Processed             bool `gorm:"default:false"`
}

func (o *OrdersCanceledForSettlement) MarshalBinary() ([]byte, error) {
	return json.Marshal(o)
}

func (o *OrdersCanceledForSettlement) UnmarshalBinary(data []byte) (*OrdersCanceledForSettlement, error) {
	if err := json.Unmarshal(data, &o); err != nil {
		return nil, err
	}
	return o, nil
}
