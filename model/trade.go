// Copyright © 2023 TXA PTE. LTD.

package model

import (
	"encoding/json"
	"strings"

	"sdp/types"

	"gorm.io/gorm"
)

type TradeStatus uint

const (
	TRADE_STATUS_NONE TradeStatus = iota
	TRADE_STATUS_OPEN
	TRADE_STATUS_EXECUTED
	TRADE_STATUS_REJECTED
)

type EscrowInfo struct {
	Id                 uint64                      `gorm:"type:numeric"`
	Address            string                      `json:"address"`
	BeneficiaryAddress string                      `json:"beneficiaryAddress"`
	NetworkType        string                      `json:"networkType"`
	ChainType          types.AssetNetworkChainType `json:"chainType"`
}

type Trade struct {
	BuyerBaseEscrow     EscrowInfo `gorm:"embedded;embeddedPrefix:buyer_base_"`
	BuyerCounterEscrow  EscrowInfo `gorm:"embedded;embeddedPrefix:buyer_counter_"`
	Price               string     `gorm:"type:numeric"`
	Product             string
	SdpSequenceNumber   uint64     `gorm:"type:numeric;primaryKey;autoIncrement:false"`
	SellerBaseEscrow    EscrowInfo `gorm:"embedded;embeddedPrefix:seller_base_"`
	SellerCounterEscrow EscrowInfo `gorm:"embedded;embeddedPrefix:seller_counter_"`
	Size                string     `gorm:"type:numeric"`
	Timestamp           uint64     `gorm:"type:numeric"`
	Processed           bool       `gorm:"default:false"`
	TradeStatus         TradeStatus
}

type RejectedTrade struct {
	Trade
}

type LastProcessedTrade struct {
	gorm.Model
	SdpSequenceNumber uint64 `gorm:"type:numeric"`
}

func (t *Trade) Validate() bool {
	return true
}

func (t *Trade) MarshalBinary() ([]byte, error) {
	return json.Marshal(t)
}

func (t *Trade) UnmarshalBinary(data []byte) (*Trade, error) {
	if err := json.Unmarshal(data, &t); err != nil {
		return nil, err
	}
	return t, nil
}

func (t *Trade) Rejected() bool {
	return t.TradeStatus == TRADE_STATUS_REJECTED
}

func (t *Trade) GetBuyerCounterAddress() string {
	return strings.ToLower(t.BuyerCounterEscrow.BeneficiaryAddress)
}

func (t *Trade) GetBuyerBaseAddress() string {
	return strings.ToLower(t.BuyerBaseEscrow.BeneficiaryAddress)
}

func (t *Trade) GetSellerCounterAddress() string {
	return strings.ToLower(t.SellerCounterEscrow.BeneficiaryAddress)
}

func (t *Trade) GetSellerBaseAddress() string {
	return strings.ToLower(t.SellerBaseEscrow.BeneficiaryAddress)
}
