// Copyright © 2023 TXA PTE. LTD.

package constants

import (
	"strings"
)

// Addresses
const (
	LockedFundsAddress string = "0x0000000000000000000000000000000000000000"
	ZeroAddress        string = "0x0000000000000000000000000000000000000000"
)

var (
	DmcAddress        = strings.ToLower("0xa85233C63b9Ee964Add6F2cffe00Fd84eb32338f")
	UsdtGoerliAddress = strings.ToLower("0x30dFf5e179450C7D434A8D3A637CB6e9e7798e23")
)

// Supported products
const (
	ETHUSDT  = string("ETH/USDT")
	ETHBTC   = string("ETH/BTC")
	BTCUSDT  = string("BTC/USDT")
	ETHDMC   = string("ETH/DMC")
	BNBBUSD  = string("BNB/BUSD")
	MATICETH = string("MATIC/ETH")
)

// Supported Chain IDs:
const (
	CLIQUEBAIT_ONE = string("17001")
	CLIQUEBAIT_TWO = string("17002")
	ETH_DEV        = string("420123")
	MATIC_DEV      = string("420124")
	BSC_DEV        = string("420125")
	ARB_DEV        = string("420126")
	GOERLI         = string("5")
	BSC_TESTNET    = string("97")
	MATIC_TESTNET  = string("80001")
	ARB_TESTNET    = string("421613")
)

var Chains = []string{CLIQUEBAIT_ONE, CLIQUEBAIT_TWO, ETH_DEV, GOERLI, BSC_DEV, BSC_TESTNET, MATIC_DEV, MATIC_TESTNET, ARB_DEV, ARB_TESTNET}

const MAX_CONNECTION_RETRIES = 20

type Worker uint

const (
	TRADE_WORKER Worker = iota
	ORDER_WORKER
	EVENT_WORKER
)

const (
	TRADE_CHANNEL = "trade_channel"
	ORDER_CHANNEL = "order_channel"
	EVENT_CHANNEL = "event_channel"
)
