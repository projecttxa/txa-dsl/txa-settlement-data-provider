image: golang:1.19

include:
  - project: "ProjectTXA/txa-utils/txa-ci-templates"
    file: "/templates/gpg-validate.yml"
  - project: "ProjectTXA/txa-utils/txa-ci-templates"
    file: "/templates/docker.yml"

variables:
  GPG_FILE: "settlements.csv"
  IMAGE_NAME: "$CI_REGISTRY_IMAGE/txa-settlement-data-provider"

.base-rules:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

.set-tags: &set-tags
  - IMAGE_TAG_LATEST="$IMAGE_NAME:latest" && \
  - echo Using image name $IMAGE_NAME && \
  - echo Using image tag $IMAGE_TAG_LATEST

stages:
  - validate
  - test
  - build
  - publish

gpg validate:
  stage: validate
  extends:
    - .gpg-validate

unit tests:
  stage: test
  extends:
    - .base-rules
  services:
    - name: postgres:14.1
      alias: postgres
    - name: docker.io/bitnami/redis:6.2
      alias: redis
  variables:
    POSTGRES_USER: "postgres"
    POSTGRES_PASSWORD: "password"
    ALLOW_EMPTY_PASSWORD: "yes"
    REDIS_DISABLE_COMMANDS: "FLUSHDB,FLUSHALL"
    REDIS_HOST: "redis"
    REDIS_PORT: "6379"
    PSQL_HOST: postgres
    PSQL_PASSWORD: password
  before_script:
    - make deps
  script:
    - make test

build image:
  stage: build
  extends:
    - .docker-base
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    - *set-tags
    - docker build --tag $IMAGE_NAME .
    - mkdir image
    - docker save $IMAGE_NAME > image/sdp.tar
  artifacts:
    untracked: true
    expire_in: 60 minutes
    paths:
      - image

publish image:
  stage: publish
  before_script:
    - docker load -i image/sdp.tar
  extends:
    - .docker-base
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_PROTECTED == "true"
  script:
    - apk --update add --no-cache git curl

    - git status
    - echo "CI_COMMIT_REF_NAME is $CI_COMMIT_REF_NAME"

    - git switch $CI_COMMIT_REF_NAME || git switch -c $CI_COMMIT_REF_NAME
    - echo "Most recent tag is $(git describe --tags)"
    - echo "All tags found are $(git tag)"
    - echo "Project path is $CI_PROJECT_PATH"

    - if [[ '$(git tag | grep -iE "[0-9]+\.[0-9]+\.[0-9]+$")' == "" ]]; then
        echo "$COMMIT_REF_NAME must contain at least one tag of the format x.x.x.  If this is your first time using \
              this publish stage, ask a repo maintainer to push a version tag to $COMMIT_REF_NAME and try again.";
        exit 1;
      fi

    - if [[ '$(git describe --tags | grep -iE "[0-9]+\.[0-9]+\.[0-9]+")' == "" ]]; then
        echo "Invalid tag found $(git describe --tags). Exiting." && exit 1;
      fi

    - if [[ -z ${AUTOTAG_TOKEN}+x ]]; then
        echo "AUTOTAG_TOKEN (read_api, read_user, api) must be set." && exit 1;
      fi

    - if git remote | grep tag-origin > /dev/null; then
        git remote rm tag-origin;
      fi
    - git remote add tag-origin https://oauth2:${AUTOTAG_TOKEN}@gitlab.com/${CI_PROJECT_PATH}

    - curl -sL https://git.io/autotag-install | sh -s -- -b $CI_PROJECT_PATH

    - AUTOTAG_VERSION=""

    - LAST_COMMIT_SHA=`git rev-parse --short=8 HEAD`

    - if [[ "$CI_COMMIT_REF_NAME" == "develop" ]]; then
        AUTOTAG_VERSION=$($CI_PROJECT_PATH/autotag -n --empty-version-prefix --branch $CI_COMMIT_REF_NAME);
      fi

    - if [[ "$CI_COMMIT_REF_NAME" == hotfix/* ]]; then
        MOST_RECENT_TAG=$(git describe --match='[0-9]*.[0-9]*.[0-9]*' --exclude='*[^0-9.]*' --tags | cut -d "-" -f1);
        AUTOTAG_VERSION=$MOST_RECENT_TAG-hotfix-$LAST_COMMIT_SHA;
      fi

    - if [[ -z ${AUTOTAG_VERSION+x} ]]; then echo "Autotag version is empty, failing the sanity check.  Contact DevOps."; exit 1; fi

    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

    - echo $AUTOTAG_VERSION
    - if [[ $(docker manifest inspect $CI_REGISTRY_IMAGE:$AUTOTAG_VERSION) ]]; then echo "$CI_REGISTRY_IMAGE:$AUTOTAG_VERSION already exists" && exit 1; fi

    - git tag $AUTOTAG_VERSION

    - git push tag-origin $AUTOTAG_VERSION -o ci.skip

    - docker tag $IMAGE_NAME "$IMAGE_NAME:$AUTOTAG_VERSION"
    - docker tag $IMAGE_NAME $IMAGE_NAME:latest

    - docker push $IMAGE_NAME --all-tags
  after_script:
    - docker image rm -f $IMAGE_NAME
