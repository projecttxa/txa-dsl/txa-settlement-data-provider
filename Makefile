THIS := $(lastword $(MAKEFILE_LIST))
SERVICES := postgres redis

.PHONY: help dev clean deps generate test test-race postgres redis cliquebait streamer indexer

help: ## Prints available targets
	@cat $(MAKEFILE_LIST) | grep -E '^[a-zA-Z_-]+:.*?## .*$$' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

deps: ## Installs dependencies
	@go get -v -d ./...

generate: ## Generate the protobuf lib
	@protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative proto_def/SDP.proto

dev: ## Run with local environment configuration
	@for service in $(SERVICES); do \
		docker ps | grep -q txa-sdp_$$service || $(MAKE) -f $(THIS) $$service; \
	done
	@TEST=1 SENTRY_ENABLED=1 APP_ENV=development go run main.go

clean: ## Teardown docker containers and remove binary
	@for service in $(SERVICES); do \
		docker kill txa-sdp_$$service || true; \
	done

test: ## Runs testing suite with race detector enabled
	@TEST=1 go test -count=1 -race ./... | sed 's/PASS/'`printf "\033[32mPASS\033[0m"`'/g' | sed 's/FAIL/'`printf "\033[31mFAIL\033[0m"`'/g'; \
	exit $?

postgres: ## Run a postgres container
	@docker run --name=txa-sdp_postgres --rm -itd -p 5432:5432 -e POSTGRES_PASSWORD=password postgres:latest

redis: ## Run a redis container
	@docker run --name=txa-sdp_redis --rm -itd -p 6379:6379 -e REDIS_PASSWORD=password -e REDIS_DISABLE_COMMANDS=FLUSHDB,FLUSHALL bitnami/redis:latest

cliquebait: ## Run a cliquebait container
	@docker run --name=txa-sdp_cliquebait --rm -itd -p 8545:8545 registry.gitlab.com/projecttxa/txa-dsl/txa-settlement-contracts/txa-settlement-contracts-cliquebait:0.8.12

streamer: ## Run a streamer container
	@echo "NOT YET IMPLEMENTED"

indexer: ## Run an indexer
	@echo "NOT YET IMPLEMENTED"
