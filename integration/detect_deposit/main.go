// Copyright © 2023 TXA PTE. LTD.

package main

import (
	"log"
	"math/big"
	"strings"

	"sdp/config"
	"sdp/constants"
	"sdp/db"
	"sdp/model"

	bn "github.com/ethereum/go-ethereum/common/math"
	"gorm.io/gorm"
)

func getDb() *gorm.DB {
	pgURL := config.GetPgURL()

	pg, _ := db.NewDb(pgURL)
	return pg
}

func queryObligations(db *gorm.DB, wallet string, token string, chainId uint64) *big.Int {
	var obligations []model.Obligation
	db.Where(&model.Obligation{Sink: wallet, Source: wallet, Token: token, ChainId: chainId}).Find(&obligations)
	if len(obligations) > 1 {
		log.Fatalf("Should never be more than one entry for the same source, sink, token, and chainId")
	}
	if len(obligations) == 0 {
		return big.NewInt(0)
	}
	return bn.MustParseBig256(obligations[0].Amount)
}

func main() {
	pg := getDb()

	// query for native token balances
	expectedAmount := "1256000000000000000"
	alice := strings.ToLower("0xEE214aC2A18bCDf7F563f63a2Cbc30DdAD9DB2E4")
	bob := strings.ToLower("0x537B304C9B632FFE222C222CD455d2bC6ae6af1f")
	chainId := uint64(17001)
	aliceEther := queryObligations(pg, alice, constants.ZeroAddress, chainId)
	if aliceEther.Cmp(bn.MustParseBig256(expectedAmount)) != 0 {
		log.Printf("Expected balance: %v", expectedAmount)
		log.Printf("Actual balance: %v", aliceEther)
		log.Fatalf("Alice's ether balance is incorrect")
	}

	aliceDmc := queryObligations(pg, alice, constants.DmcAddress, chainId)
	if aliceDmc.Cmp(bn.MustParseBig256(expectedAmount)) != 0 {
		log.Printf("Expected balance: %v", expectedAmount)
		log.Printf("Actual balance: %v", aliceDmc)
		log.Fatalf("Alice's ether balance is incorrect")
	}

	bobEther := queryObligations(pg, bob, constants.ZeroAddress, chainId)
	if bobEther.Cmp(bn.MustParseBig256(expectedAmount)) != 0 {
		log.Printf("Expected balance: %v", expectedAmount)
		log.Printf("Actual balance: %v", bobEther)
		log.Fatalf("Bob's ether balance is incorrect")
	}

	bobDmc := queryObligations(pg, bob, constants.DmcAddress, chainId)
	if bobDmc.Cmp(bn.MustParseBig256(expectedAmount)) != 0 {
		log.Printf("Expected balance: %v", expectedAmount)
		log.Printf("Actual balance: %v", bobDmc)
		log.Fatalf("Bob's ether balance is incorrect")
	}
}
