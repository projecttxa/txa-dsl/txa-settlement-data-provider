#!/bin/sh
asdf=$(docker-compose -f docker-compose.test.yml logs cliquebait)

while ! echo ${asdf} | grep "initialized predeployed contracts" > /dev/null;
do
    sleep 5
    echo "Waiting for contracts to be initialized in cliquebait..."
    asdf=$(docker-compose -f docker-compose.test.yml logs cliquebait)
done
