#!/bin/sh
asdf=$(kubectl logs -n exchange deployments/exchange-sdp)
echo $asdf

while ! echo ${asdf} | grep "Transaction hash" >/dev/null; do
  sleep 5
  echo "Waiting for SDO to detect trade messages..."
  echo ${asdf}
  asdf=$(kubectl logs -n exchange deployments/exchange-sdp)
done
