#!/bin/sh
asdf=$(kubectl logs -n exchange deployments/exchange-sdp)
echo $asdf

while ! echo ${asdf} | grep "finished processing trade: 1500" >/dev/null; do
  sleep 5
  echo "Waiting for SDO to detect trade messages..."
  echo ${asdf}
  asdf=$(kubectl logs -n exchange deployments/exchange-sdp)
done
