// Copyright © 2023 TXA PTE. LTD.

package worker

import (
	"context"

	"sdp/constants"
	"sdp/logger"

	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
)

type DbWorker struct {
	closing chan chan error
	worker  constants.Worker
	store   Store
	cache   Cache

	logger *logrus.Logger
}

type Store interface {
	Create(any) error
}

type Cache interface {
	Create(any) error
	Subscribe(context.Context, string) *redis.PubSub
}

func NewDbWorker(w constants.Worker, store Store, cache Cache) *DbWorker {
	return &DbWorker{
		closing: make(chan chan error),
		store:   store,
		cache:   cache,
		worker:  w,

		logger: logger.Default,
	}
}

func (w *DbWorker) Close() error {
	errc := make(chan error)
	w.closing <- errc
	return <-errc
}

func (w *DbWorker) Start(ctx context.Context) {
	decode := decoders[w.worker]
	pubsub := w.cache.Subscribe(ctx, keys[w.worker])
	messages := pubsub.Channel()

	var err error
	var decoded any

	defer pubsub.Close()
	for {
		select {
		case <-ctx.Done():
			return
		case errc := <-w.closing:
			errc <- err
			return
		case message, ok := <-messages:
			if !ok {
				return
			}
			decoded, err = decode(message.Payload)
			if err != nil {
				w.logException("error decoding message: %v", err)
			}
			if err := w.cache.Create(decoded); err != nil {
				w.logException("error caching message: %v", err)
			}
			if err := w.store.Create(decoded); err != nil {
				w.logException("error recording message: %v", err)
			}
		}
	}
}

func (w *DbWorker) logException(str string, err error) {
	logger.CaptureException(err)
	w.logger.Errorf(str, err)
}
