// Copyright © 2023 TXA PTE. LTD.

package worker

import (
	"bytes"
	"encoding/json"
	"sync"

	"sdp/config"
	"sdp/constants"
	"sdp/model"
	"sdp/proto_def"
	"sdp/types"

	"google.golang.org/protobuf/encoding/prototext"
)

var bufferPool = sync.Pool{
	New: func() interface{} {
		return new(bytes.Buffer)
	},
}

type tradeMessage struct {
	MsgType types.MsgType
	Trade   *model.Trade
}

type orderMessage struct {
	MsgType        types.MsgType
	OrdersCanceled *model.OrdersCanceledForSettlement
}

type Decodeable interface {
	*model.Event | *model.OrdersCanceledForSettlement | *model.Trade
}

type DecoderFunc func(msg string) (any, error)

var decoders = map[constants.Worker]DecoderFunc{
	constants.TRADE_WORKER: decodeTrade,
	constants.ORDER_WORKER: decodeOrder,
	constants.EVENT_WORKER: decodeBlockchainEvent,
}

var keys = map[constants.Worker]string{
	constants.TRADE_WORKER: config.GetTradeQueueKey(),
	constants.ORDER_WORKER: config.GetOrderQueueKey(),
	constants.EVENT_WORKER: config.GetBlockchainEventsKey(),
}

func decodeMessage[T Decodeable](msg string, decoder func(buf *bytes.Buffer) (T, error)) (T, error) {
	buf := bufferPool.Get().(*bytes.Buffer)
	buf.Reset()
	buf.Write([]byte(msg))

	result, err := decoder(buf)
	bufferPool.Put(buf)
	return result, err
}

func decodeTrade(msg string) (any, error) {
	return decodeMessage(msg, func(buf *bytes.Buffer) (*model.Trade, error) {
		trade := &tradeMessage{}
		if err := json.NewDecoder(buf).Decode(trade); err != nil {
			return nil, err
		}
		return trade.Trade, nil
	})
}

func decodeOrder(msg string) (any, error) {
	return decodeMessage(msg, func(buf *bytes.Buffer) (*model.OrdersCanceledForSettlement, error) {
		order := &orderMessage{}
		if err := json.NewDecoder(buf).Decode(order); err != nil {
			return nil, err
		}
		return order.OrdersCanceled, nil
	})
}

func decodeBlockchainEvent(msg string) (any, error) {
	return decodeMessage(msg, func(buf *bytes.Buffer) (*model.Event, error) {
		event := &proto_def.ParsedEventMessage{}
		m_event := &model.Event{}
		if err := prototext.Unmarshal(buf.Bytes(), event); err != nil {
			return nil, err
		}
		return m_event.Decode(event), nil
	})
}
