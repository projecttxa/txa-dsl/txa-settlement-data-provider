// Copyright © 2023 TXA PTE. LTD.

package logger

import (
	"time"

	"sdp/config"

	"github.com/getsentry/sentry-go"
	"github.com/sirupsen/logrus"
)

var logger = Default

type sentryClient struct {
	logger *logrus.Logger
}

func NewSentry() *sentryClient {
	if !config.GetSentryEnabled() {
		return nil
	}
	if err := sentry.Init(sentry.ClientOptions{
		Dsn:   config.GetSentryDsn(),
		Debug: true,
	}); err != nil {
		logger.Fatal(err)
	}
	return &sentryClient{}
}

func (s *sentryClient) Flush(t time.Duration) {
	if !config.GetSentryEnabled() {
		return
	}
	sentry.Flush(t)
}

func CaptureException(err error) {
	if !config.GetSentryEnabled() {
		return
	}
	hub := sentry.CurrentHub().Clone()
	hub.CaptureException(err)
}

func CaptureMessage(msg string) {
	if !config.GetSentryEnabled() {
		return
	}
	hub := sentry.CurrentHub().Clone()
	hub.CaptureMessage(msg)
}
